#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  7 14:23:19 2018

@author: stefan
"""

from DSMC import DSMC, time_to_str
from local_paths import *
import os
import numpy as np
import matplotlib.pyplot as plt

t0 = 7*24+0.5
t1 = 17*24+9
# MRC
config = {}
config['velocity_nc_files'] = area_p_velocity_nc_files
config['results_dir'] = area_p_results_dir[:-1] + '_hyper/'
config['run'] = -1
config['search_method'] = 'hypergraph'

config['vel_data_trim'] = [5, 31, 85, 107, -42, -23] # days, days, lon, lon, lat lat
config['samples_bounding_polygon'] = np.loadtxt('area_p_splash_polygon.txt')
#config['number_of_samples'] = 100000
config['samples_density'] = 2 #samples per km^2

config['advection_time'] = np.arange(t0, 31*24+1e-3, 1.0) # Hours

config['X'] = np.linspace(88, 106, 721)
config['Y'] = np.linspace(-39, -26, 521)

dsmc = DSMC()     
dsmc.detection_substeps = 25
dsmc.init_case(config)
dsmc.velocity_initialization()
dsmc.samples_advection()
dsmc.calculate_domain_hypergraph(t1, t0)

