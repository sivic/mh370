#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 14:20:49 2018

@author: stefan
"""

import numpy as np
from netCDF4 import Dataset
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import scipy.interpolate as sp_intrp
import scipy.integrate as integ 
import scipy.optimize as opt
from scipy.fftpack import dct, idct
import os
import ghalton
from shapely.geometry import Point, Polygon, MultiPoint, LineString
from shapely import affinity
from descartes.patch import PolygonPatch
import sys
import datetime
from scipy.ndimage.filters import gaussian_filter
import time as timer
import copy as cp

def DCT(field):
    return dct(dct(field.T, norm='ortho').T, norm='ortho')

def IDCT(coefficients):
    return idct(idct(coefficients.T, norm='ortho').T, norm='ortho')

#time0 = datetime.datetime(2014, 3, 7, 0, 30)
time0 = datetime.datetime(2014, 3, 1, 0, 0)
def time_to_str(time):    
    time = time0 + datetime.timedelta(hours=time)
    return time.strftime('%Y-%m-%d %H:%M')
    
def log_title(title):
    print('\n' + '*' * 80 + '\n' + title.upper().center(80) + '\n' + '*' * 80)
    
class DSMC:
    
    def __init__(self):
        # Initialization
        
        # Velocity data
        self.vel_loaded = False
        self.vel_time = None
        self.vel_x = None
        self.vel_y = None
        self.vel_vx = None
        self.vel_vy = None
        self.C=None
        self.M=None
        self.X= None
        self.Y= None
        self.detection_substeps = 5
        
        
    def init_case(self, config):
            
        log_title('Init case')
        if 'label' not in config:
            config['label'] = ''
        self.config = config    
        
        print(config['results_dir'] + config['label'])
        
        if self.config['search_method'] == 'mdsmc':
            config['smoothing_exponent'] = 0.5
        else:
            config['smoothing_exponent'] = 1.5
            
        self.full_run = self.config['run'] < 0
        if self.config['run'] < 0:
            if config['search_method'] == 'hypergraph':
                self.run_dir = self.config['results_dir'] 
            else:
                self.run_dir = self.config['results_dir'] + '/%s%s_run_%s/' % (self.config['label'], self.config['search_method'], 'full')
        else:
            self.run_dir = self.config['results_dir'] + '/%s%s_run_%03d/' % (self.config['label'], self.config['search_method'], self.config['run'])
        
        if not os.path.exists(self.run_dir):            
            os.makedirs(self.run_dir)            
        if not os.path.exists(self.run_dir + 'frames/'):
            os.makedirs(self.run_dir + 'frames/')
            
        self.X = self.config['X']
        self.Y = self.config['Y']
        self.dx = self.X[1] - self.X[0]
        self.dy = self.Y[1] - self.Y[0]
    
        if config['search_method'] != 'hypergraph': 
            t1, t2, dt, snt = self.config['search_time']
            print(f' t1: {t1}')
            print(f' t2: {t2}')
            print(f' dt: {dt}')
            print(f' snt: {snt}')
            nt = int(np.round((t2 - t1) * 3600 / dt) + 1)
            print(f' nt: {snt}')
            self.search_time = np.linspace(t1 * 3600, t2 * 3600, nt) / 3600.0 # [hours]
            if snt == 0:
                snt = nt - 1
            self.SIT = np.arange(0, nt, snt)
            self.search_step = dt / 3600.0 # [hours]
            
            self.detection_rate = 0.0
            self.detection_history = np.zeros_like(self.search_time)
            print(self.search_time)
            print(self.SIT)
            print(f'self.search_time.shape: {self.search_time.shape}')
            #input(' >> Press return to continue')
        
        self.method = self.config['search_method']
        
        
    def create_common_files(self, config):
        
        self.config = config
        self.X = self.config['X']
        self.Y = self.config['Y']
        self.dx = self.X[1] - self.X[0]
        self.dy = self.Y[1] - self.Y[0]
        
        self.run_dir = self.config['results_dir'] 
       
        self.velocity_initialization()
        self.samples_advection()
        
        #xmg, ymg = np.meshgrid(self.X, self.Y)
        np.savez_compressed(self.config['results_dir'] + 'common_results.npz',
                            x = self.X,
                            y = self.Y,
                            m0 = self.mu(0, self.X, self.Y))

    
    def velocity_initialization(self):  
        
        log_title('velocity initialization')
        
        if not os.path.exists(self.config['results_dir'] + 'velocity.npz'):
            # Read raw velocity data from NetCDF files if there is no .npz files (containing trimmed velocity data)
            for i, nc_file in enumerate(self.config['velocity_nc_files']):
                if i == 0:
                    self.read_velocity_from_nc(nc_file)
                else:
                    self.read_velocity_from_nc(nc_file, True)
                               
            t1, t2, x1, x2, y1, y2 = self.config['vel_data_trim']
            self.trim_velocity_data(t1, t2, x1, x2, y1, y2)   
            self.convert_velocity_to_deg_per_hour()          
            self.write_velocity_to_npz(self.config['results_dir'] + 'velocity.npz')
        
        else:
            self.read_velocity_from_npz(self.config['results_dir'] + 'velocity.npz')
        
        self.init_velocity_field_interpolation()   
        
        
    def ghalton_generate(self,bounding_polygon,number_of_samples):
        print('Initializing samples')
        print('  Total samples:', number_of_samples)
        sequencer = ghalton.Halton(2)
                    
        points = sequencer.get(number_of_samples)
        
        polygon = Polygon(bounding_polygon)
        x1, y1, x2, y2 = polygon.bounds
        
        samples = []
        for x, y in points:
            x = x1 + x * (x2 - x1)
            y = y1 + y * (y2 - y1)
        
            if polygon.contains(Point(x, y)):
                samples.append([x, y])
        return samples
    
    
    def constant_density_ghalton(self,bounding_polygon,samples_density):
        print('Initializing samples')
        polygon = Polygon(bounding_polygon)
        x1, y1, x2, y2 = polygon.bounds
        R = 6371
        sequencer = ghalton.Halton(2)
        
        y, x = np.meshgrid(self.vel_y, self.vel_x) 
        scale_x = R * np.cos(np.deg2rad(y1)) * np.pi / 180.0
        dx1=scale_x*(x2-x1)
        scale_x = R * np.cos(np.deg2rad(y2)) * np.pi / 180.0
        dx2=scale_x*(x2-x1)
        dx=max(dx1,dx2)
        scale_y = R * np.pi / 180.0
        dy=(y2-y1)*scale_y

        number_of_samples=int(samples_density*dy*dx)
        #print('number_of_samples',number_of_samples,dy,dx,samples_density)
        points = sequencer.get(number_of_samples)
        samples = []
        #print('dx/dx1',dx/dx1,'dx/dx2',dx/dx2)
        for x, y in points:
            x=x*(dx/dx1*(1.-y)+dx/dx2*y)
            #if x>1.:continue
            x = x1 + x * (x2 - x1)
            y = y1 + y * (y2 - y1)
            if polygon.contains(Point(x, y)):
                samples.append([x, y])
        print('  Total samples:', len(samples))
        return samples   

     
    def samples_advection(self):
        
        log_title('samples advection')
        
        time = self.config['advection_time']        
        print('Advection time interval: [%f, %f] (%s, %s)' % (time[0], time[-1], time_to_str(time[0]), time_to_str(time[-1])))
        print('Advection time step: %f hours' % (time[1] - time[0]))
        
        if not os.path.exists(self.config['results_dir'] + '/advection.npz'):       
            # If there are no avaliable results for samples advection -> Do the advection calculation
            
            samples_density = self.config['samples_density']
            bounding_polygon = self.config['samples_bounding_polygon']           
            samples=self.constant_density_ghalton(bounding_polygon,samples_density)
            
            self.S0 = np.array(samples)
            
            plt.figure()
            plt.scatter(self.S0[:,0], self.S0[:,1], s=0.5, alpha=0.2)
            X, Y = bounding_polygon.T
            for i, (x, y) in enumerate(zip(X,Y)):
                plt.plot(x,y,'ro')
                plt.text(x,y,str(i+1))
            plt.savefig(self.config['results_dir'] + 'samples.png')
            plt.close()
            
            print('  Samples used:', np.shape(self.S0))
            print('Advecting...')  
                        
            ns = np.shape(self.S0)[0]            
            xy0 = np.zeros(ns * 2)
            xy0[0::2] =  self.S0[:,0]
            xy0[1::2] =  self.S0[:,1]
            
            SXY = integ.odeint(self.dxy_dt, xy0, time, rtol=0.0, atol=1.e-5)
            
            self.SX = SXY[:,0::2]
            self.SY = SXY[:,1::2]
            self.ST = time
            
            print('  Samples advection done!')        
            print('Saving advected samples to', self.config['results_dir'] + 'advection.npz')
            np.savez_compressed(self.config['results_dir'] + 'advection.npz', st=self.ST, sx=self.SX, sy=self.SY)
            print('  Done')
        
        else:
            # Read samples advection results if avaliable
            
            print('Samples advection results avaliable')
            print('Reading samples advection from', self.config['results_dir'] + 'advection.npz')
            npz = np.load(self.config['results_dir'] + 'advection.npz')
            self.ST = npz['st']
            self.SX = npz['sx']
            self.SY = npz['sy']
            self.S0 = np.vstack([self.SX[0,:], self.SY[0,:]]).T
            print('  Samples used:', np.shape(self.S0))
            print('  Done')
            #input(' >> Press return to continue.')
            
            
        print('Advected samples bounds: [%f, %f] x [%f, %f]' % (np.min(self.SX), np.max(self.SX), np.min(self.SY), np.max(self.SY)))
        #input(' > Press return to continue')
        
        # Create functions for obtaining samples positions at any time
        def sx(t):
            it2 = np.argmax(self.ST > t)
            #it2 = np.argmin(np.abs(self.ST - t))
            #it2 = np.min(np.where(self.ST >= t))
            if self.ST[it2] < t:
                it2 = it2 + 1 
            it1 = it2 - 1
            
            w1 = (self.ST[it2] - t) / (self.ST[it2] - self.ST[it1])
            w2 = (t - self.ST[it1]) / (self.ST[it2] - self.ST[it1])
            
            sx = self.SX[it1,:] * w1 + self.SX[it2,:] * w2    
            return sx
        
        def sy(t):
            it2 = np.argmax(self.ST > t)
            #it2 = np.argmin(np.abs(self.ST - t))
            #it2 = np.min(np.where(self.ST >= t))
            if self.ST[it2] < t:
                it2 = it2 + 1 
            it1 = it2 - 1
            
            w1 = (self.ST[it2] - t) / (self.ST[it2] - self.ST[it1])
            w2 = (t - self.ST[it1]) / (self.ST[it2] - self.ST[it1])
            
            sy = self.SY[it1,:] * w1 + self.SY[it2,:] * w2    
            return sy
        
        # Samples distribution
        def mu(t, xh, yh):
            it2 = np.argmax(self.ST > t)
            #it2 = np.argmin(np.abs(self.ST - t))
            #it2 = np.min(np.where(self.ST >= t))
            if self.ST[it2] < t:
                it2 = it2 + 1 
            it1 = it2 - 1
            
            w1 = (self.ST[it2] - t) / (self.ST[it2] - self.ST[it1])
            w2 = (t - self.ST[it1]) / (self.ST[it2] - self.ST[it1])
            
            sx = self.SX[it1,:] * w1 + self.SX[it2,:] * w2
            sy = self.SY[it1,:] * w1 + self.SY[it2,:] * w2
            
            m, _, _ = np.histogram2d(sy, sx, bins=[yh, xh])
            return m
        
        
        self.sx_interp = sp_intrp.interp1d(self.ST, self.SX[:,0])
        self.sx = sx
        self.sy = sy
        self.mu = mu
            
    
    def targets_advection(self):
        
        log_title('targets advection')
        
        time = self.config['advection_time']        
        print('Advection time interval: [%f, %f] (%s, %s)' % (time[0], time[-1], time_to_str(time[0]), time_to_str(time[-1])))
        print('Advection time step: %f hours' % (time[1] - time[0]))
        
        print('Checking results in ' + self.run_dir + '/targets.npz')
        if not os.path.exists(self.run_dir + '/targets.npz'):
            
            print('Targets initialization')
            self.number_of_targets = self.config['number_of_targets']            
            ns, _ = np.shape(self.S0)
            
            I = np.random.permutation(ns)[:self.number_of_targets]
            self.T0 =  self.S0[I,:] + np.random.normal(0, 1e-4, (self.number_of_targets,2))
            print('  %d targets initialized' % self.number_of_targets)
            
            print('Targets advection')
            print('  Time interval: [%f, %f] (%s, %s)' % (time[0], time[-1], time_to_str(time[0]), time_to_str(time[-1])))
            
            ns = np.shape(self.T0)[0]
            xy0 = np.zeros(ns * 2)
            xy0[0::2] =  self.T0[:,0]
            xy0[1::2] =  self.T0[:,1]
            
            # args tahes sigma for uncertainty 
            if self.config['uncertainty_sigma'] > 0:
                TXY = self.euler_method(self.dxy_dt, xy0, time)
            else:
                TXY = integ.odeint(self.dxy_dt, xy0, time, rtol=0.0, atol=1.e-5)
            
            self.TX = TXY[:,0::2]
            self.TY = TXY[:,1::2]
            self.TT = time
            
            print('  Target advection done!')            
            print('Saving advected targets to', self.run_dir + 'targets.npz')
            np.savez_compressed(self.run_dir + 'targets.npz', st=self.TT, sx=self.TX, sy=self.TY)  
            print('  Done')
        
        else:
            
            print('Target advection results avaliable')
            print('Reading target advection from', self.run_dir + '/targets.npz')
            npz = np.load(self.run_dir + '/targets.npz')
            self.TT = npz['st']
            self.TX = npz['sx']
            self.TY = npz['sy']
            self.T0 = np.vstack([self.TX[0,:], self.TY[0,:]]).T
            self.number_of_targets = np.shape(self.TX)[1]
            npz.close()
            print('  Done')
            #print(self.TT[0], self.TX[0,0], self.TY[0,0])
            #input(' >> Press return to continue.')
            
        
        def tx(t):
            it2 = np.argmax(self.TT > t)
            #it2 = np.argmin(np.abs(self.ST - t))
            #it2 = np.min(np.where(self.ST >= t))
            if self.TT[it2] < t:
                it2 = it2 + 1 
            it1 = it2 - 1
            
            w1 = (self.TT[it2] - t) / (self.TT[it2] - self.TT[it1])
            w2 = (t - self.TT[it1]) / (self.TT[it2] - self.TT[it1])
            
            tx = self.TX[it1,:] * w1 + self.TX[it2,:] * w2    
            return tx
        
        def ty(t):
            #it2 = np.argmax(self.ST > t)
            it2 = np.argmin(np.abs(self.TT - t))
            if self.TT[it2] < t:
                it2 = it2 + 1 
            it1 = it2 - 1
            w1 = (t - self.TT[it1]) / (self.TT[it2] - self.TT[it1])
            w2 = (self.TT[it2] - t) / (self.TT[it2] - self.TT[it1])
            
            ty = self.TY[it1,:] * w1 + self.TY[it2,:] * w2    
            return ty
        
        self.tx = tx
        self.ty = ty
        
    def agent_trail_advection(self, t, dt):
        na = np.size(self.AAX)    
        if na == 0:
            return
        
        #print('agent_trail_advection', na)
        xy0 = np.zeros(na * 2)
        xy0[0::2] =  self.AAX
        xy0[1::2] =  self.AAY
        
        AXY = integ.odeint(self.dxy_dt, xy0, [t, t+dt], rtol=0.0, atol=1.e-5)
        
        self.AAX = AXY[-1,0::2]
        self.AAY = AXY[-1,1::2]
        
        #print('advection', np.shape(self.AAX), np.shape(self.AAY))
        
        
    def run_search(self):
         
        log_title('running search')
        
        total_start_time = timer.time()
            
        nx = np.size(self.X)
        ny = np.size(self.Y)
        
        self.xh = np.linspace(self.X[0] - 0.5*self.dx, self.X[-1]+0.5*self.dx, nx+1)
        self.yh = np.linspace(self.Y[0] - 0.5*self.dy, self.Y[-1]+0.5*self.dy, ny+1)
                
        ky, kx = np.meshgrid(np.arange(ny), np.arange(nx))
        gamma = self.config['smoothing_exponent'] 
        #self.La = 1.0 / (1 + kx**2 + ky**2) ** 1.5
        self.La = 1.0 / (1 + kx**2 + ky**2) ** gamma
        
        dt = self.search_step
        
        R = 6371.0 
        va = self.config['agents_velocity'] * 3.6 # in km/hr
        self.detection_probability = 1.0 - np.exp(-dt / self.detection_substeps * 3600.0 / self.config['detection_time'])
        self.detection_distance = self.config['detection_radius'] / R / np.pi * 180.0 
        print('dt: %f seconds' % (dt*3600))
        print('dt: %f minutes' % (dt*60))
        print('Detection probability (per second):', 1.0 - np.exp(-1 / self.config['detection_time']))
        print('Detection probability (per minute):', 1.0 - np.exp(-60 / self.config['detection_time']))
        print('Detection probability (per time step):', 1.0 - np.exp(-dt*3600 / self.config['detection_time']))
        #input(' >> Press return to continue')        
        
        self.AAX = np.array([])
        self.AAY = np.array([])
        self.AX = np.array([])
        self.AY = np.array([])
        
        self.target_status = np.zeros(self.config['number_of_targets'], dtype=np.bool)
        self.undetected = np.arange(self.config['number_of_targets'], dtype=np.int)
        self.detected = np.array([], dtype=np.int)
        self.detection_rate = 0.0
        
        if not os.path.exists(self.run_dir + '/iterations/'):
            os.makedirs(self.run_dir + '/iterations/')
        
        
        self.search_active = np.zeros_like(self.search_time, dtype=np.bool)
        reinit_agents = np.array([], dtype=np.int)
        
        for regime, (t1, t2, number_of_agents) in enumerate(self.config['search_regime']):
            
            it1 = np.argmin(np.abs(t1 - self.search_time*60))
            it2 = np.argmin(np.abs(t2 - self.search_time*60))
            
            #print('regime', t1, t2, number_of_agents, it1, it2)
            self.search_active[it1:it2+1] = True
            reinit_agents = np.append(reinit_agents, it1)
        
        #print('reinitialize agents:', reinit_agents)
        #input(' >> Press return to continue')        
        
        self.regime = -1
        
                
        for it, t in enumerate(self.search_time):
            
            start_time = timer.time()
                
            if self.search_active[it]:
                            
                if it in reinit_agents:
                    """
                    Reinitialization of agents (at the start of each search period/day)
                    """
                    self.regime += 1
                    _, _, number_of_agents = self.config['search_regime'][self.regime]
                    
                    if self.method == 'dsmc' or self.method == 'mdsmc':
                        I = np.random.permutation(np.shape(self.S0)[0])[:number_of_agents]
                        self.AX =  self.sx(t)[I] #+ np.random.normal(0, 1e-4, (number_of_agents))
                        self.AY =  self.sy(t)[I] #+ np.random.normal(0, 1e-4, (number_of_agents))
                    elif self.method == 'lwnmwrpredef':
                        hull_points = self.config['hull_points'][self.regime*2:self.regime*2+2,:]
                        self.init_lawnmower_convexhull(t, number_of_agents, hull_points)
                        self.AX, self.AY = self.lawnmower_coordinates(t)
                    elif self.method == 'lwnmwrhull':
                        self.init_lawnmower_convexhull(t, number_of_agents)
                        self.AX, self.AY = self.lawnmower_coordinates(t)
                    else:
                        raise ValueError('Unknown search method (%s)' % self.method)
                
                
                    if self.method == 'dsmc' or self.method == 'mdsmc':
                        
                        self.calc_potential(t)
                        
                    self.detection_history[it] = self.detection_rate
                
                else:
                    
                    if self.method == 'dsmc' or self.method == 'mdsmc':
                        
                        self.calc_potential(t)
                        un = np.sqrt(self.ux**2 + self.uy**2)
                        
                        vax = sp_intrp.interp2d(self.X, self.Y, self.ux/un, kind='linear')
                        vay = sp_intrp.interp2d(self.X, self.Y, self.uy/un, kind='linear')
                  
                        scale_y = R * np.pi / 180.0
                        AVX, AVY = [], []
                        
                        for ia, (xa, ya) in enumerate(zip(self.AX, self.AY)):
                            
                            scale_x = R * np.cos(np.deg2rad(ya)) * np.pi / 180.0 
                            va_x = vax(xa, ya)[0] * va / scale_x
                            va_y = vay(xa, ya)[0] * va / scale_y
                            AVX.append(va_x)
                            AVY.append(va_y)
                        
                        AVX = np.array(AVX)
                        AVY = np.array(AVY)
                        
                        #self.target_detection(it)
                        self.target_detection(self.AX, self.AY, self.AX + AVX*dt, self.AY + AVY*dt, t-0.5*dt, it)
                        
                        self.AX += AVX * dt
                        self.AY += AVY * dt
                
                    elif self.method == 'lwnmwrpredef':
                        
                        ax, ay = self.lawnmower_coordinates(t)
                        self.target_detection(self.AX, self.AY, ax, ay, t-0.5*dt, it)
                        self.AX, self.AY = ax, ay
                        
                    elif self.method == 'lwnmwrhull':
                        
                        ax, ay = self.lawnmower_coordinates(t)
                        self.target_detection(self.AX, self.AY, ax, ay, t-0.5*dt, it)
                        self.AX, self.AY = ax, ay
                        
                    else:
                        raise ValueError('Unknown search method')
                        
                
                self.agent_trail_advection(t, dt)
                
                self.AAX = np.append(self.AAX, self.AX)
                self.AAY = np.append(self.AAY, self.AY)
                
                print("%.3f SEARCH %s detection: %.1f%% exec. time: %.6f seconds" % (t, time_to_str(t), self.detection_rate, timer.time() - start_time))
            
            else:
                if it > 0  and not self.full_run:
                    if self.search_active[it-1]:
                        ITS = np.where(self.search_active)[0]
                        #print(ITS)
                        it_next_active = ITS[np.argmax(ITS>it)]
                        #print(it_next_active)
                        time_step = (it_next_active - it) * dt
                                                
                        self.agent_trail_advection(t, time_step)
                        #print(time_step, t + time_step, time_to_str(t + time_step))
                        #input(' >> Press return to continue.')
                else:
                    self.agent_trail_advection(t, dt)
                    
                    
                self.detection_history[it] = self.detection_rate
                print("%.3f NOSEARCH %s detection: %.1f%% exec. time: %.6f seconds" % (t, time_to_str(t), self.detection_rate, timer.time() - start_time))
        
        
            if (self.search_active[it] and it in self.SIT) or (self.full_run and it in self.SIT):
                
                if not self.search_active[it] and (self.method == 'dsmc' or self.method == 'mdsmc'):
                    self.calc_potential(t)
                    
                sit = np.where(self.SIT == it)
                
                if it % (6*60) == (6*30) or not self.full_run:
                    self.plot_search(it)
                
                
                npz_filename = self.run_dir + '/iterations/search_%010d.npz' % sit
                if self.method == 'dsmc' or self.method == 'mdsmc':
                    np.savez_compressed(npz_filename, 
                                        t=t, 
                                        it=it, 
                                        AX=self.AX, 
                                        AY=self.AY, 
                                        AAX=self.AAX, 
                                        AAY=self.AAY,
                                        U=self.u,
                                        Ux=self.ux,
                                        Uy=self.uy,
                                        detected=self.detected,
                                        undetected=self.undetected,
                                        detection_rate=self.detection_rate)
                else:
                    np.savez_compressed(npz_filename, 
                                        t=t, 
                                        it=it, 
                                        AX=self.AX, 
                                        AY=self.AY, 
                                        AAX=self.AAX, 
                                        AAY=self.AAY,
                                        detected=self.detected,
                                        undetected=self.undetected,
                                        detection_rate=self.detection_rate)
                        
        np.savez_compressed(self.run_dir + '/convergence.npz',
                            search_time=self.search_time,
                            detection_history=self.detection_history)
        
        print("Total search exec. time: %s seconds" % (timer.time() - total_start_time))
        #self.plot_search(np.size(self.config['search_time'])-1)
    
        #plt.figure()
        #plt.plot(time, np.mean(self.TS,1))
    
    def calc_potential(self, t):
        
        sigma = self.config['sigma']
        
        # Target probability
        self.MU = self.mu(t, self.xh, self.yh)            
        self.MU = gaussian_filter(self.MU, sigma=[sigma/self.dx, sigma/self.dy])
        self.MU /= np.sum(self.MU) # Normalization, this is same as trapz (since mu=0 at the boundary) on unit domain 
        
        mu = np.log(self.MU+1e-9)
        
        
        
        # Agents coverage
        self.C, _, _ = np.histogram2d(self.AAY, self.AAX, bins=[self.yh, self.xh])
        self.C = gaussian_filter(self.C, sigma=[sigma/self.dx, sigma/self.dy])  
        # Coverage is represented by number of agent trail points in each domain cell
        # Total coverage  is integral (sum if boundary cells are empty) of self.C
        
        T = 1.0 # Search duration [hours] for which "coverage capacity" is calculated
        # It cuts off target distribution which can't be covered with this capacity
        # (only higher probabilites remain)
        # Higher value directs the search towards infinite search
        
        #C0 = np.size(self.AX) * 3.0 / self.search_step
        C0 = np.sum(self.C) + np.size(self.AX) * T / self.search_step
        #C0 = 1e3
        
        def sub_plus(f):
            return 0.5 * (f + np.abs(f))
        def opt_dist(alpha):
            return sub_plus(mu - alpha)
        def e_int(alpha):
            return C0 - np.sum(opt_dist(alpha))
        
        alpha = opt.fsolve(e_int, -10)
        #self.S = opt_dist(alpha) - self.C
        if self.method == 'mdsmc':
            self.S = mu - alpha - self.C
            self.S[self.S<0] = 0.0
        if self.method == 'dsmc':
            csum = np.sum(self.C)
            if csum > 0.0:
                self.S = self.MU - self.C / csum
            else:
                self.S = self.MU
            #self.S[self.S<0] = 0.0
        
        """
        print('C0: %e' % C0)
        print('Alpha: e_int(%e)=%e' % (alpha, e_int(alpha)))
        print('p: [%e, %e]' % (np.min(self.MU), np.max(self.MU)))
        print('mu: [%e, %e]' % (np.min(mu), np.max(mu)))
        print('c: [%e, %e]' % (np.min(self.C), np.max(self.C)))
        print('s: [%e, %e]' % (np.min(self.S), np.max(self.S)))
        """
        """
        if np.sum(self.C) > 0.0:
            self.C /= np.sum(self.C)
        
            self.S = self.MU - self.C
            self.S[self.S<0] = 0.0
            
        else:
            self.S = self.MU
        """ 
 
        Las = self.La * (DCT(self.S)).T
        self.u = IDCT(Las).T
        
        self.uy, self.ux = np.gradient(self.u)
    
    def target_detection(self, ax1, ay1, ax2, ay2, t, it):
        """
        Target detection                
        """
        tx = self.tx(t)[self.undetected]
        ty = self.ty(t)[self.undetected]
        yscale = np.cos(np.deg2rad(ty))**2
        all_detected = np.array([], dtype=np.int)
        
        #for ax, ay in zip(self.AX, self.AY):
        
        for tt in np.linspace(0, 1, self.detection_substeps+1)[1:]:
            AX = ax1 + tt * (ax2 - ax1)
            AY = ay1 + tt * (ay2 - ay1)
            
            for ax, ay in zip(AX, AY):
                d = np.sqrt(yscale * (tx - ax)**2 + (ty - ay)**2)
                candidates = self.undetected[np.where(d < self.detection_distance)]
                nc = np.size(candidates) 
                    
                detected = np.random.uniform(0, 1, nc) < self.detection_probability
                #self.TS[it:,candidates] = detected
                all_detected = np.append(all_detected, candidates[detected])
            
        all_detected = np.unique(all_detected)
        self.target_status[all_detected] = True
        #self.undetected = np.delete(self.undetected, all_detected, axis=None)
        #self.detected = np.append(self.detected, all_detected)
        self.detected = np.where(self.target_status)[0]
        self.undetected = np.where(np.logical_not(self.target_status))[0]
        #self.detection_rate = (np.size(self.detected)/self.number_of_targets) * 100.0
        self.detection_rate = np.average(self.target_status) * 100
        #print(np.size(self.undetected), np.size(self.detected), np.size(self.undetected) + np.size(self.detected), all_detected)   
        #input()
    
        self.detection_history[it] = self.detection_rate
        
        
    
    def plot_search(self, it):
        
        t = self.search_time[it]
        sit = np.argmin(np.abs(self.SIT - it))
        
        plt.figure(figsize=(14, 14))
        plt.title('%s (%.1f%% detection)' % (time_to_str(t), self.detection_rate))
        #plt.contourf(self.X, self.Y, self.u, alpha=0.3)
        #plt.colorbar()
        #qn = 2
        #plt.quiver(self.X[::qn], self.Y[::qn], self.ux[::qn,::qn], self.uy[::qn,::qn], alpha=.2, scale=100)
        #plt.contourf(self.X, self.Y, self.MU, alpha=1, cmap=plt.cm.GnBu)
        
        plt.scatter(self.sx(t), self.sy(t), s=0.01, c='grey', alpha=0.2)
        
        
        plt.scatter(self.tx(t)[self.undetected], self.ty(t)[self.undetected], s=6.0, c='red', alpha=0.8)
        plt.scatter(self.tx(t)[self.detected], self.ty(t)[self.detected], s=50.0, c='green', alpha=1.0, marker='x')
        
        plt.scatter(self.AAX, self.AAY, s=0.2, c='purple')
        plt.scatter(self.AX, self.AY, s=20.0, c='purple')
        
        plt.xlim(self.X[0], self.X[-1])
        plt.ylim(self.Y[0], self.Y[-1])
        plt.axis('equal')
        
        plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95)
        plt.savefig(self.run_dir + 'frames/search_%010d.png' % sit, dpi=300)
        plt.close()
        
        
        if self.method == 'dsmc' or self.method == 'mdsmc':
            plt.figure(figsize=(14, 14))
            plt.title('%s' % (time_to_str(t)))
            
            gs = gridspec.GridSpec(2, 2)
            ax1 = plt.subplot(gs[0, 0])
            ax2 = plt.subplot(gs[0, 1])
            ax3 = plt.subplot(gs[1, 0])
            ax4 = plt.subplot(gs[1, 1])
            
            cntf = ax1.contourf(self.X, self.Y, self.C)
            ax1.set_xlim(self.X[0], self.X[-1])
            ax1.set_ylim(self.Y[0], self.Y[-1])
            ax1.axis('equal')
            ax1.set_title('C')
            plt.colorbar(cntf, ax=ax1)
            
            cntf = ax2.contourf(self.X, self.Y, np.log(self.MU+1e-9))
            ax2.set_xlim(self.X[0], self.X[-1])
            ax2.set_ylim(self.Y[0], self.Y[-1])
            ax2.axis('equal')
            ax2.set_title('MU')
            plt.colorbar(cntf, ax=ax2)
            
            cntf = ax3.contourf(self.X, self.Y, self.S)
            ax3.set_xlim(self.X[0], self.X[-1])
            ax3.set_ylim(self.Y[0], self.Y[-1])
            ax3.axis('equal')
            ax3.set_title('S')
            plt.colorbar(cntf, ax=ax3)
            
            cntf = ax4.contourf(self.X, self.Y, self.u)
            ax4.set_xlim(self.X[0], self.X[-1])
            ax4.set_ylim(self.Y[0], self.Y[-1])
            ax4.axis('equal')
            ax4.set_title('U')
            plt.colorbar(cntf, ax=ax4)
            
            plt.subplots_adjust(left=0.05, right=0.95, bottom=0.05, top=0.95)
            plt.savefig(self.run_dir + 'frames/fields_%010d.png' % sit, dpi=300)
            plt.close()
        
    def visualize(self):
        
        time = self.search_time[::10]
        """
        sx = np.array([])
        sy = np.array([])
        
        for it, t in enumerate(time):
            sx = np.append(sx, self.sx(t)[0])
            sy = np.append(sy, self.sy(t)[0])
        plt.figure()
        plt.plot(time, sx)
        plt.plot(time, self.sx_interp(time))
        #plt.plot(time, sy)
        plt.savefig(self.run_dir + 'frames/adv_test.png')
        """
        
        for it, t in enumerate(time):
            
            print('  Making frame %d %f (%s)' % (it, t, time_to_str(t)))
            plt.figure(figsize=(10,10)) 
            #self.plot_velocity_field(plt.gca(), t)
            plt.scatter(self.sx(t)[:], self.sy(t)[:], c='b', s=1, alpha=0.1) 
            plt.scatter(self.tx(t)[:], self.ty(t)[:], c='r', s=1, alpha=0.8) 
            plt.title('Time: %s' % time_to_str(t))
            
            
            plt.subplots_adjust(left=0.05, right=0.98, bottom=0.05, top=0.95)
            plt.savefig(self.run_dir + 'frames/adv_%06d.png' % it)
            plt.close()
            
        
    def make_figures(self, config):
        
        self.config = cp.deepcopy(config)
        self.X = self.config['X']
        self.Y = self.config['Y']
        self.dx = self.X[1] - self.X[0]
        self.dy = self.Y[1] - self.Y[0]
        
        self.run_dir = self.config['results_dir'] 
       
        
        
        self.velocity_initialization()
        self.samples_advection()
        
        
        import seaborn as sns
        sns.set_style("darkgrid")
            
            
        # Splash plot
        if self.config['case'] == 'Area A':
            plt.figure(figsize=(6, 5.3), dpi=200)
            plt.subplots_adjust(left=0.08, bottom=0.015, right=0.97, top=0.985, wspace=0.15, hspace=0.15)
            t0 = 27*24+6
            t1= 31*24+9
        elif self.config['case'] == 'Area B':
            plt.figure(figsize=(6, 3.2), dpi=200)
            plt.subplots_adjust(left=0.08, bottom=0.015, right=0.97, top=0.985, wspace=0.15, hspace=0.15)
            t0 = 32*24+6
            t1 = 33*24+9
        
        print(f't0: {t0}, {time_to_str(t0)}')
        print(f't1: {t1}, {time_to_str(t1)}')
        #input(' >> Press return to continue.')
        ax = plt.subplot()
        self.plot_samples(ax, t1)
        self.plot_initial_area(ax)
        self.format_axis(ax)
        ax.set_title('Drifted target probability distribution on ' + time_to_str(t0), fontsize=10)
        plt.savefig(config['results_dir'] + config['case'].lower().replace(' ', '_') + '_splash.png')
        plt.close()
        
        # Search plot
        if self.config['case'] == 'Area A':
            plt.figure(figsize=(12, 11), dpi=200)
            gs = gridspec.GridSpec(2, 2, width_ratios=[1,1], height_ratios=[1,1])
            plt.subplots_adjust(left=0.04, bottom=0.02, right=0.98, top=0.98, wspace=0.15, hspace=0.15)
        if self.config['case'] == 'Area B':
            plt.figure(figsize=(12, 6.5), dpi=200)
            gs = gridspec.GridSpec(2, 2, width_ratios=[1,1], height_ratios=[1,1])
            plt.subplots_adjust(left=0.04, bottom=0.02, right=0.98, top=0.98, wspace=0.15, hspace=0.15)
        
        
        methods = 'lwnmwrpredef lwnmwrhull dsmc mdsmc'.split()
        labels = 'ABCD'
        titles = ['Lawnmower scenario 1 search on ' + time_to_str(t1),
                  'Lawnmower scenario 2 search on ' + time_to_str(t1),
                  'DSMC search on ' + time_to_str(t1),
                  'mDSMC search on ' + time_to_str(t1),
                  ]
        for iax in range(4):
            
            ax = plt.subplot(gs[iax])
            
            cpconfig = cp.deepcopy(config)
            cpconfig['run'] = -1
            cpconfig['search_method'] = methods[iax]
            
            dsmc = DSMC()
            dsmc.init_case(cpconfig)
            self.plot_samples(ax, t1)
            
            dsmc.targets_advection()
            dsmc.plot_agents(ax, t1)
            dsmc.plot_targets(ax, t1)
            
            self.format_axis(ax)
            ax.text(0, 1.005, labels[iax], 
                    fontsize=14,
                    fontweight = 'bold',
                    horizontalalignment='left',
                    verticalalignment='bottom', 
                    transform=ax.transAxes)
            
            ax.set_title(titles[iax], fontsize=10)
            
        print(config['results_dir'])
        plt.savefig(config['results_dir'] + config['case'].lower().replace(' ', '_') + '_search.png')
        plt.close()
        
    def plot_samples(self, ax, t, ms=1, alpha=0.01):
        
        print(f'Plot samples (t={t}, {time_to_str(t)})')
        ax.set_title(f'Probability density drift, {time_to_str(t)}')
        #ax.contour(x, y, m, 100, zorder=0)
        #sns.kdeplot(sx, sy, ax=ax, cmap="Blues", shade=True, shade_lowest=False)
        #ax.scatter(self.sx(t), self.sy(t), s=0.2, alpha=0.01, color='steelblue', zorder=3)
        ax.plot(self.sx(t), self.sy(t), '.', ms=ms, alpha=alpha, color='steelblue', zorder=10, mec=None)
        
    def plot_initial_area(self, ax):
        
        if self.config['case'] == 'Area A':
            SP = np.array([
            [96.3925681965,	-33.85301067],
            [98.4352402091,	-31.82609007],
            [97.3236915587,	-29.62465096],
            [95.8137037302,	-31.34907281]
            ])
        elif self.config['case'] == 'Area B':
            SP = np.loadtxt('area_b_splash_polygon.txt')
        elif self.config['case'] == 'Area P':
            SP = np.loadtxt('area_p_splash_polygon.txt')
        
        polygon = Polygon(SP)
        patch1 = PolygonPatch(polygon, facecolor='purple', edgecolor=None, alpha=0.3, zorder=2)
        patch2 = PolygonPatch(polygon, fill=None, edgecolor='black', alpha=0.3, zorder=3, linewidth=1)
        ax.add_patch(patch1)
        ax.add_patch(patch2)
        
        if not self.config['case'] == 'Area P':
            lwnfile = open(self.config['case'].lower().replace(' ', '_') + '_lawnmower.txt')
            lines = lwnfile.readlines()
            lwnfile.close()
            
            for i in range(0, len(lines), 3):
                x = [float(val) for val in lines[i+1].split()]
                y = [float(val) for val in lines[i+2].split()]
                xy = np.vstack((x, y)).T
                #print(np.shape(xy))
                #input(' >> Press return to continue.')
                polygon = Polygon(xy)
                patch = PolygonPatch(polygon, facecolor='grey', edgecolor=None, alpha=0.2, zorder=50)
                ax.add_patch(patch)
        
    def plot_agents(self, ax, t, ms=0.1):
        
        print(f'Plot agents (t={t}, {time_to_str(t)})')
        it = np.argmin(np.abs(self.search_time - t))
        print(f'self.search_time.shape: {self.search_time.shape}')
        print(f'self.search_time: [{self.search_time[0]}, {self.search_time[-1]}]')
        print(it)
        print(self.SIT)
        print(f'SIT.shape: {self.SIT.shape}')
        sit = np.where(self.SIT == it)       
        print(sit)
        npz_filename = self.run_dir + '/iterations/search_%010d.npz' % sit
        if not os.path.exists(npz_filename):
            return
        npz = np.load(npz_filename)
        #print(npz['AAX'].shape)
        #print(npz['AX'].shape)
        AAX = npz['AAX']
        AAY = npz['AAY']
        AX = npz['AX']
        AY = npz['AY']
        npz.close()
        
        ax.plot(AAX[::2], AAY[::2],'.', ms=ms, color='r', alpha=1, zorder=20, mec=None)
        #ax.scatter(AX, AY, marker='o', s=5, color='r', alpha=0.7)
        
    def plot_targets(self, ax, t):
        
        print(f'Plot targets (t={t}, {time_to_str(t)})')
        print(self.search_time)
        print(self.SIT)
        it = np.argmin(np.abs(self.search_time - t))
        sit = np.where(self.SIT == it)       
        npz_filename = self.run_dir + '/iterations/search_%010d.npz' % sit
        print(f'Reading from {npz_filename}')
        if not os.path.exists(npz_filename):
            print('No results file:', npz_filename)
            return
        npz = np.load(npz_filename)
        detected = npz['detected']
        undetected = npz['undetected']
        npz.close()
        
        print(f'detected: {np.sum(detected)}')
        print(f'undetected: {np.sum(undetected)}')
        #print(self.tx(t)[undetected])
        #input(' >> Press return to continue.')
        dr = 100 * np.sum(detected) / (np.sum(detected) + np.sum(undetected))
        ax.text(0.995, 0.005, f'Detection rate: {dr:.1f}%', 
                fontsize=10,
                color='green',
                #fontweight = 'bold',
                horizontalalignment='right',
                verticalalignment='bottom', 
                transform=ax.transAxes)
        
        ax.plot(self.tx(t)[undetected], self.ty(t)[undetected], '+', 
                color='k', alpha=0.75, ms = 2, zorder=40)  
        ax.plot(self.tx(t)[detected], self.ty(t)[detected], 'x', 
                color='limegreen', alpha=0.75, ms = 4, zorder=40)    
        
    def read_velocity_from_nc(self, nc_path, append=True):
        # Read velocity data from NetCDF file
        print('Reading velocity data from '+ nc_path)
        nc_velocity = Dataset(nc_path)
        
        if self.vel_loaded and append:
            
            self.vel_time = np.append(self.vel_time, np.array(nc_velocity['time']))
            self.vel_x = np.array(nc_velocity['x_axis'])
            self.vel_y = np.array(nc_velocity['y_axis'])
            self.vel_vx = np.append(self.vel_vx, np.array(nc_velocity['U']), 0)
            self.vel_vy = np.append(self.vel_vy, np.array(nc_velocity['V']), 0)
        
        else: 
            
            self.vel_time = np.array(nc_velocity['time'])
            self.vel_x = np.array(nc_velocity['x_axis'])
            self.vel_y = np.array(nc_velocity['y_axis'])
            
            self.vel_vx = np.swapaxes(np.array(nc_velocity['U']), 1, 2)
            self.vel_vy = np.swapaxes(np.array(nc_velocity['V']), 1 ,2)
                    
        self.vel_loaded = True
        nc_velocity.close()
        print('  Reading done!')
    
    def trim_velocity_data(self, t1, t2, x1, x2, y1, y2):
        
        print('Trimming velocity data')
        print('  Initial data shape:', np.shape(self.vel_vx))
        print('  t: [%f, %f] ==> [%f, %f] (%s, %s)' % (self.vel_time[0], self.vel_time[-1], t1, t2, time_to_str(t1*24), time_to_str(t2*24)))
        print('  x: [%f, %f] ==> [%f, %f]' % (self.vel_x[0], self.vel_x[-1], x1, x2))
        print('  y: [%f, %f] ==> [%f, %f]' % (self.vel_y[0], self.vel_y[-1], y1, y2))
        
        it = np.where(np.logical_and(t1 <= self.vel_time, self.vel_time <= t2))[0]
        ix = np.where(np.logical_and(x1 <= self.vel_x, self.vel_x <= x2))[0]
        iy = np.where(np.logical_and(y1 <= self.vel_y, self.vel_y <= y2))[0]
        
        self.vel_time = self.vel_time[it]
        self.vel_x = self.vel_x[ix]
        self.vel_y = self.vel_y[iy]
                
        self.vel_vx = self.vel_vx[it,:,:]
        self.vel_vy = self.vel_vy[it,:,:]
                
        self.vel_vx = self.vel_vx[:,ix,:]
        self.vel_vy = self.vel_vy[:,ix,:]
                
        self.vel_vx = self.vel_vx[:,:,iy]
        self.vel_vy = self.vel_vy[:,:,iy]
        
        print('  Final data shape:', np.shape(self.vel_vx))
        
    def convert_velocity_to_deg_per_hour(self):
        # scales the velocity field
        #scales = 86400./111200.*np.sqrt(self.vel_vy**2+(self.vel_vx/np.cos(self.vel_y*np.pi/180.))**2);
        print('Converting velocities to deg/hr')
        R = 6371000.0
        secs_in_hour = 60.0 * 60.0
        y, x = np.meshgrid(self.vel_y, self.vel_x) 
        scale_x = R * np.cos(np.deg2rad(y)) * np.pi / 180.0 
        scale_y = R * np.pi / 180.0
        
        for i in range(np.size(self.vel_time)):
            self.vel_vx[i,:,:] = self.vel_vx[i,:,:] / scale_x * secs_in_hour
            self.vel_vy[i,:,:] = self.vel_vy[i,:,:] / scale_y * secs_in_hour
            
        self.vel_time = 24.0 * self.vel_time
        
    def write_velocity_to_npz(self, npz_path):
        # Writes velocity data to npz file
        print('Saving velocity data to ' + npz_path)
        np.savez_compressed(npz_path,
                            time = self.vel_time,
                            x = self.vel_x,
                            y = self.vel_y,
                            vx = self.vel_vx,
                            vy = self.vel_vy)
        print('  Saving done!')
        
    def read_velocity_from_npz(self, npz_path):
        # Writes velocity data to npz file
        print('Reading velocity data from ' + npz_path)
        npz = np.load(npz_path)
            
        self.vel_time = npz['time']
        self.vel_x = npz['x']
        self.vel_y = npz['y']
        self.vel_vx = npz['vx']
        self.vel_vy = npz['vy']
        print('  Loading done!')
        self.vel_loaded = True
        npz.close()
        


    def init_velocity_field_interpolation(self):
        
        print('Initializing velocity field interpolation\n')
        
        print('    t', self.vel_time.shape, self.vel_time.min(), self.vel_time.max())
        print('    t', self.vel_time.shape, time_to_str(self.vel_time.min()), time_to_str(self.vel_time.max()))
        print('    x', self.vel_x.shape, self.vel_x.min(), self.vel_x.max())
        print('    y', self.vel_y.shape, self.vel_y.min(), self.vel_y.max())
        print('    vx', self.vel_vx.shape, self.vel_vx.min(), self.vel_vx.max())
        print('    vy', self.vel_vy.shape, self.vel_vy.min(), self.vel_vy.max())
        
        self.vx = sp_intrp.RegularGridInterpolator((self.vel_time, self.vel_x, self.vel_y), self.vel_vx, bounds_error=False, fill_value=None)
        self.vy = sp_intrp.RegularGridInterpolator((self.vel_time, self.vel_x, self.vel_y), self.vel_vy, bounds_error=False, fill_value=None)
        
        
        def v(t, x, y):
            #print(t,x,y)
            vx = self.vx([t, x, y])
            vy = self.vy([t, x, y])
            
            return np.array([vx, vy])
        
        def v_list(t, x, y):
            
            points = list(zip(t,x,y))
            #print(points)
            vx = self.vx(points)
            vy = self.vy(points)
            
            return np.array([vx, vy]).T
        
        def v_array(t, x, y):
            ni, nj = np.shape(x)
            vx = np.zeros_like(x)
            vy = np.zeros_like(x)            
            for i in range(ni):
                for j in range(nj):
                    pt = [t, x[i,j], y[i,j]]
                    vx[i,j] = self.vx(pt)
                    vy[i,j] = self.vy(pt)
            return vx, vy
        
        self.v = v
        self.v_list = v_list
        self.v_array = v_array
            
        print('    Done\n')
        
    def plot_velocity_field(self, ax, time):
        
        #qs = 500
        #qx = self.vel_x[::qs]
        #qy = self.vel_y[::qs]
        #qvx = self.vel_vx[time,::qs,::qs]
        #qvy = self.vel_vy[time,::qs,::qs]
        
        qx = np.linspace(np.min(self.vel_x), np.max(self.vel_x), 121)
        qy = np.linspace(np.min(self.vel_y), np.max(self.vel_y), 121)
        qx, qy = np.meshgrid(qx, qy)
        
        qvx, qvy = self.v_array(time, qx, qy)
        
        #print(qvx.min(), qvx.max())
        """
        qvx = np.zeros_like(qx)
        qvy = np.zeros_like(qx)
        ni, nj = np.shape(qx)       
        for i in range(ni):
            for j in range(nj):
                pt = [time, qx[i,j], qy[i,j]]
                qvx[i,j] = self.vx(pt)
                qvy[i,j] = self.vy(pt)
        """
        
        ax.quiver(qx, qy, qvx, qvy,
                  units='inches',
                  scale=.2)
        
        #ax.set_title('Interpolated')
        
        
    def plot_velocity_field_from_data(self, ax, it):
                
        qn = 2
        qx = self.vel_x[::qn]
        qy = self.vel_y[::qn]
        qx, qy = np.meshgrid(qx, qy, indexing='ij')

        qvx = self.vel_vx[it,::qn,::qn]
        qvy = self.vel_vy[it,::qn,::qn]
        
        ax.quiver(qx, qy, qvx, qvy,
                  units='inches',
                  scale=.2)
        
        ax.set_title('Data')
     
        
    def dxy_dt(self, xy, t, uncertainty_sigma=0):
        #print('dxy_dt', xy, t)
        x = xy[0::2]
        y = xy[1::2]
        
        tt=t*np.ones_like(x)
    
        vx = self.vx(np.column_stack((tt, x, y)))
        vy = self.vy(np.column_stack((tt, x, y)))
        
        #vx = np.array([self.vx([t, xx, yy]) for (xx, yy) in zip(x, y)])[:,0]
        #vy = np.array([self.vy([t, xx, yy]) for (xx, yy) in zip(x, y)])[:,0]
       
        if uncertainty_sigma > 0:
            #print(f'uncertainty_sigma: {uncertainty_sigma}')
            #input(' >> Press return to continue.')
            
            x1m = 1.0 / (111111.0 * np.cos(y / 180 * np.pi))
            y1m = 1.0 / 111111.0
            
            _vx = np.random.normal(loc=0, scale=1, size=np.shape(vx))
            _vy = np.random.normal(loc=0, scale=1, size=np.shape(vy))
            _vx[_vx < -5] = -5 
            _vx[_vx > 5] = 5 
            _vy[_vy < -5] = -5 
            _vy[_vy > 5] = 5 
            
            
            #_vx = np.random.uniform(-1, 1, np.shape(vx))
            #_vy = np.random.uniform(-1, 1, np.shape(vy))
                        
            _vx *= 3600 * np.abs(x1m) * uncertainty_sigma
            _vy *= 3600 * np.abs(y1m) * uncertainty_sigma
            
            # print(np.min(x), np.max(x), np.min(y), np.max(y))
            # print(np.min(x1m), np.max(x1m), np.min(y1m), np.max(y1m))
            # print(np.min(vx), np.max(vx), np.min(vy), np.max(vy))
            # print(np.min(_vx), np.max(_vx), np.min(_vy), np.max(_vy))
            # input(' >> Press return to continue.')

            vx += _vx
            vy += _vy

        vxy = np.zeros_like(xy)
        vxy[0::2] = vx
        vxy[1::2] = vy

        
        
        #if np.fmod(t, 1) == 0:
        #    sys.stdout.write('.')
        #    if np.fmod(t, 24) == 0:
        #        sys.stdout.write('  t: %f (%s)\n' % (t, time_to_str(t)))
        #    sys.stdout.flush() 
        
        return vxy
    
    def euler_method(self, dxy_dt, xy0, time):
        
        XY = np.zeros([np.size(time), np.size(xy0)])
        XY [0, :] = xy0
        
        for i in range(1, np.size(time)):
            XY[i, :] = XY[i - 1, :] + (time[i] - time[i - 1]) * dxy_dt(XY[i - 1, :], time[i - 1], 
                                                                              uncertainty_sigma=self.config['uncertainty_sigma'])          

        return XY
        
        
    def init_lawnmower_convexhull(self, t, number_of_agents, hull_points = None):
        
        if hull_points is None:
            points = []
            SX = self.sx(t)
            SY = self.sy(t)
            for x, y in zip(SX, SY):
                points.append((x, y))
            points = MultiPoint(points)
            
            self.lawnmower_hull = points.convex_hull            
        else:
            print('Predefined hull points:\n', hull_points)
            points = MultiPoint(hull_points.T)
            self.lawnmower_hull = points.convex_hull     

        #xyh = np.array(self.lawnmower_hull.exterior.coords)
        minx, miny, maxx, maxy = self.lawnmower_hull.bounds
        print('Lawnmower hull bounds: [%f, %f] x [%f, %f]' % (minx, maxx, miny, maxy))
        
            
        
        center = [0.5 * (self.X[0] + self.X[-1]), 0.5 * (self.Y[0] + self.Y[-1])]
        
        if hull_points is None:
            direction = np.random.uniform(0, 2*np.pi)   
        else:
            direction = 0.0
            #direction = np.random.uniform(0, 2*np.pi)   
        #dx = np.cos(direction)
        #dy = np.sin(direction)
        
        rot_hull = affinity.rotate(self.lawnmower_hull, direction, use_radians=True, origin=center)
        minx, miny, maxx, maxy = rot_hull.bounds
        print(minx, maxx, miny, maxy)
        
        R = 6371.0
        #secs_in_hour = 60.0 * 60.0
        scale_y = R * np.pi / 180.0
        
        
        def semicircle(x1, y1, x2, y2, up):
            
            #return [x1, x2], [y1, y2]
            
            if up:
                phi = np.linspace(-np.pi, 0, 46)  
            else:
                phi = np.linspace(np.pi, 0, 46)
                #phi = np.zeros(10)
            r = 0.5 * np.sqrt((x1 - x2)**2 + (y1- y2)**2) 
            xc = 0.5 * (x1 + x2)
            yc = 0.5 * (y1 + y2)
            xsc = xc + r * np.cos(phi)
            ysc = yc + r * np.sin(phi)
            #print(r)
            #print(phi)
            #print(xsc)
            #print(ysc)
            #print(x1, x2, y1, y2, up)
            return xsc, ysc
        
        #vert = np.abs(self.agents[ia]['vy']) > np.abs(self.agents[ia]['vx'])
        
        #rm = 0.1 #self.agents[ia]['rm']
        def lawnmower_maze(rm):

            round_path = True            
            DX = np.arange(minx + 0.5*rm, maxx - 0.5*rm +1e-6, 2 * rm)
            DY = np.arange(miny + rm, maxy - rm +1e-6, 2 * rm)
            #print(rm)
            #print(DX, DY)
            DX += 0.5 * (maxx - DX[-1])
            DY += 0.5 * (maxy - DY[-1])
            
            #DX += np.random.uniform() * (maxx - DX[-1] - rm)
            #DY += np.random.uniform() * (maxy - DY[-1] - rm)
        
            lwn_x, lwn_y = [], []
            
            drc_up = True #np.random.uniform() > 0.5
            
            DDX = DX
            #DDX = np.hstack([DX, DX[-2:0:-1]]) 
            #print(DX)
            #print(DDX)
            for dx in DDX:
                line = LineString([(dx, miny), (dx, maxy)])
                points = line.intersection(rot_hull)
                #print(points)            
                lwn_x.append(dx)
                lwn_x.append(dx) 
                if drc_up:
                    lwn_y.append(points.coords[0][1])
                    lwn_y.append(points.coords[1][1])
                else:
                    lwn_y.append(points.coords[1][1])
                    lwn_y.append(points.coords[0][1])
                
                drc_up = not drc_up
            
            for i in range(1, np.size(lwn_y)-2, 2):
                y_mid = 0.5 * (lwn_y[i] + lwn_y[i+1]) 
                lwn_y[i] = y_mid
                lwn_y[i+1] = y_mid
            
            if round_path:
                lwn_x_new = []
                lwn_y_new = []
                drc_up = True
                n = np.size(lwn_y)
                for i in range(0, n):
                    if i % 2 == 1 and i < n-1:
                        xsc, ysc = semicircle(lwn_x[i], lwn_y[i], lwn_x[i+1], lwn_y[i+1], drc_up)
                        lwn_x_new.extend(xsc[:-1])
                        lwn_y_new.extend(ysc[:-1])
                    else:
                        lwn_x_new.append(lwn_x[i])
                        lwn_y_new.append(lwn_y[i])
                        drc_up = not drc_up
                lwn_x = lwn_x_new
                lwn_y = lwn_y_new
                
            LWN = MultiPoint([(x,y) for x,y in zip(lwn_x, lwn_y)])
            rot_lwn = affinity.rotate(LWN, -direction, use_radians=True, origin=center)
            
            nseg = len(rot_lwn)
            lwn_x, lwn_y = [], []
            for seg in range(nseg):
                x, y = rot_lwn[seg].coords[0]
                lwn_x.append(x)
                lwn_y.append(y)
                
                    
            s = np.zeros(len(lwn_x))
            
            for i in range(1, len(lwn_x)):
                scale_x = R * np.cos(np.deg2rad(0.5 * (lwn_y[i] + lwn_y[i-1]))) * np.pi / 180.0 
                s[i] = s[i-1] + np.sqrt(scale_x**2 * (lwn_x[i] - lwn_x[i-1])**2 + scale_y**2 * (lwn_y[i] - lwn_y[i-1])**2)
            
            #print(rm, s[-1], len(lwn_x))
            #print('Agent %d: (%8.2f, %8.2f) (%8.2f, %8.2f) s:%f t_total:%f' % (ia, xa, ya, vx, vy, s, s/self.agents[ia]['va']))
            return s, DX.size, lwn_x, lwn_y
        
        
        RM = np.linspace(0.01, 0.5)
        S = np.zeros_like(RM)
        for irm, rm in enumerate(RM):
            S[irm] = lawnmower_maze(rm)[0][-1]
            
        search_duration = 3.0 # [hours]
        #number_of_agents = 5
        s_day = self.config['agents_velocity'] * 3.6 * search_duration * number_of_agents
        
        def s_diff(rm):
            s, _, _, _ = lawnmower_maze(rm)
            return s[-1] - s_day
        rm = opt.brentq(s_diff, 0.01, np.min([(maxx-minx)*0.5,(maxy-miny)*0.5]))
        print('brent done')
        s, lon_n, lwn_x, lwn_y = lawnmower_maze(rm)
        print('Maze transfersal distance:', rm)   
        print('Total length:', s[-1])    
        print('Target length:', s_day)  
        print('Number lon paths:', lon_n)  
        
        while s[-1] < 1.01 * s_day:
            print('Doing correction')
            rm = (maxx - minx) / (2 * lon_n + 2)
            s, lon_n, lwn_x, lwn_y = lawnmower_maze(rm)
            print('Maze transfersal distance:', rm)   
            print('Total length:', s[-1])    
            print('Target length:', s_day)  
            print('Number lon paths:', lon_n)   
            #DX = np.arange(minx + 0.5*rm, maxx - 0.5*rm +1e-6, 2 * rm) 
        """
        plt.figure()
        plt.plot(RM, S)
        plt.axhline(s_day)
        plt.plot(rm, s_day, 'ro')
        """
        
        lwn_t = s / (self.config['agents_velocity'] * 3.6)
        lwnx = sp_intrp.interp1d(lwn_t + t, lwn_x, fill_value='extrapolate')
        lwny = sp_intrp.interp1d(lwn_t + t, lwn_y, fill_value='extrapolate')
        
        time_offset = search_duration * s[-1] / s_day 
        print(time_offset)
        def lwn(tt):
            XA, YA = np.zeros(number_of_agents), np.zeros(number_of_agents)
            for ia in range(number_of_agents):
                ttt = tt + time_offset * ia + 0.5 * (time_offset - search_duration)
                XA[ia] = lwnx(ttt)
                YA[ia] = lwny(ttt)
            return XA, YA
        
        self.lawnmower_coordinates = lwn
        #print(lwn_t)
        
        plt.figure(figsize=(20,20))
        ax = plt.gca()
        ax.set_title('Lawnmower maze (Total length: %.2f km)' % s[-1])
        ax.scatter(self.sx(t), self.sy(t), s=0.01, c='grey', alpha=0.5)
        #ax.contourf(self.X, self.Y, self.m, np.linspace(0, self.plot_options['c_levels'][-1], 201), cmap = plt.cm.viridis, alpha=0.1)
        ring_patch = PolygonPatch(self.lawnmower_hull, facecolor='wheat', edgecolor='red', alpha=0.05, zorder=-2)
        ax.add_patch(ring_patch)
        ring_patch = PolygonPatch(self.lawnmower_hull, facecolor='None', edgecolor='red', alpha=1.0, zorder=-2)
        ax.add_patch(ring_patch)
        #ax.plot(lwn_x, lwn_y, ls='--', lw=2, c='black')
        #ax.plot(lwn_x, lwn_y, '.', lw=2, c='black')
        #ax.plot(self.LWNX[ia][self.LWNI[ia]], self.LWNY[ia][self.LWNI[ia]], 'gs')  
        
        XA = np.zeros([501, number_of_agents])
        YA = np.zeros([501, number_of_agents])
        #print(XA.shape)
        #print(YA.shape)
        for it, tt in enumerate(np.linspace(t, t+3, 501)):
            XA[it,:], YA[it,:] = lwn(tt)
        for ia in range(number_of_agents):
            ax.plot(XA[:,ia], YA[:,ia], ls='-', lw=2, label='Agent %d' % ia)
        
        ax.legend()
        ax.plot(lwn_x[0], lwn_y[0], 'go')
        ax.plot(lwn_x[-1], lwn_y[-1], 'ro')
        ax.axis('image') 
        plt.savefig(self.run_dir  + '/lawnmower_%d.png' % (self.regime)) 
        plt.close()
            
        #input('>>> press return to continue')
    
    
    
    def format_axis(self, ax):
        ax.axis('image')
        ax.set_xlim(self.X[0], self.X[-1])
        ax.set_ylim(self.Y[0], self.Y[-1])
        
        xlbl = np.arange(self.X[0], self.X[-1]+1e-3)
        ylbl = np.arange(self.Y[0], self.Y[-1]+1e-3)
        
        if xlbl.size > 15:
            xlbl = xlbl[::2]
        if ylbl.size > 15:
            ylbl = ylbl[::2]
            
        ax.set_xticks(xlbl)
        ax.set_yticks(ylbl)
        ax.set_xticklabels([u'%.0f°E' % deg for deg in ax.xaxis.get_majorticklocs()], fontsize=8)
        ax.set_yticklabels([u'%.0f°S' % -deg for deg in ax.yaxis.get_majorticklocs()], fontsize=8)
        
        ax.tick_params(direction='out', pad=7)
        plt.draw()
    
    
    def calculate_domain_hypergraph(self, t0, t1):
        
        log_title('domain hypergraph')
        #time = self.config['advection_time']   
        #time = np.arange(7*24+0.5, 31.0*24+1e-3, 1.0) # Hours
        time = np.arange(t0, t1 + 1e-3*(t1>t0), 1.0 if t1 > t0 else -1) # Hours
        nx = np.size(self.X)
        ny = np.size(self.Y)
        dx = self.X[1] - self.X[0]
        dy = self.Y[1] - self.Y[0]
        nt = np.size(time)
        T = t1 - t0
        Ts = 4./(T**2)
        
        
        xh = np.linspace(self.X[0] - 0.5*self.dx, self.X[-1]+0.5*self.dx, nx+1)
        yh = np.linspace(self.Y[0] - 0.5*self.dy, self.Y[-1]+0.5*self.dy, ny+1)
        
        X0, Y0 = np.meshgrid(xh, yh)
        
        
        #time = self.config['advection_time']        
        print('Advection time interval: [%f, %f] (%s, %s)' % (time[0], time[-1], time_to_str(time[0]), time_to_str(time[-1])))
        print('Advection time step: %f hours' % (time[1] - time[0]))
        
        if not os.path.exists(self.config['results_dir'] + '/domain_hypergraph_advection.npz'):       
            
            
            S0 = np.zeros([(nx+1)*(ny+1),2])
            S0[:,0] = X0.reshape(-1)
            S0[:,1] = Y0.reshape(-1)
            
            print('  Samples used:', np.shape(S0))
            print('Advecting...')  
                        
            ns = np.shape(S0)[0]
            
            xy0 = np.zeros(ns * 2)
            xy0[0::2] =  S0[:,0]
            xy0[1::2] =  S0[:,1]
            
            SXY = integ.odeint(self.dxy_dt, xy0, time, rtol=0.0, atol=1.e-5)
            
            SX = SXY[:,0::2].reshape([nt, ny+1, nx+1])
            SY = SXY[:,1::2].reshape([nt, ny+1, nx+1])
            ST = time
            
            print('  Samples advection done!')     
            
            print('Saving advection results to', self.config['results_dir'] + '/domain_hypergraph_advection.npz')
            np.savez_compressed(self.config['results_dir'] + '/domain_hypergraph_advection.npz', 
                                st=ST, sx=SX, sy=SY)
        
        else:
            print('Samples advection results avaliable')
            print('Reading samples advection from', self.config['results_dir'] + '/domain_hypergraph_advection.npz')
            npz = np.load(self.config['results_dir'] + '/domain_hypergraph_advection.npz')
            ST = npz['st']
            SX = npz['sx']
            SY = npz['sy']
            print(ST.shape)
            print(SX.shape)
            print(SY.shape)
            

            #input(' >> Press return to continue.')
            
        if not os.path.exists(self.config['results_dir'] + '/domain_hypergraph.npz'):     
            # Calculate mesochronic velocities
            print('  Calculating mesochronic velocities')
            X1 = SX[-1,:,:]
            Y1 = SX[-1,:,:]
            Dx = X1 - X0
            Dy = Y1 - Y0
            
            mVx = Dx / T
            mVy = Dy / T
            
            # Jacobian components
            vx_y, vx_x = np.gradient(mVx, dy, dx)
            vy_y, vy_x = np.gradient(mVy, dy, dx)
            
            # Determinant
            DET = vx_x * vy_y - vx_y * vy_x
            
            # Adjust determinat values for plotting
            D = np.copy(DET)
            D[DET > Ts] = Ts + np.log(1.0 - Ts + DET[DET > Ts])
            D[DET < 0] = - np.log(1.0 - DET[DET < 0])
            
            print('Saving hypergraph results to', self.config['results_dir'] + '/domain_hypergraph.npz')
            np.savez_compressed(self.config['results_dir'] + '/domain_hypergraph.npz', 
                                mVx=mVx, mVy=mVy,
                                vx_x=vx_x, vx_y=vx_y, vy_x=vy_x, vy_y=vy_y,
                                DET=DET, D=D)
            
            print('  Done')
        
        else:
            
            print('Samples advection results avaliable')
            print('Reading samples advection from', self.config['results_dir'] + '/domain_hypergraph.npz')
            npz = np.load(self.config['results_dir'] + '/domain_hypergraph.npz')
            mVx = npz['mVx']
            mVy = npz['mVy']
            vx_x = npz['vx_x']
            vx_y = npz['vx_y']
            vy_x = npz['vy_x']
            vy_y = npz['vy_y']
            DET = npz['DET']
            D = npz['D']
            
            S0 = np.vstack([SX[0,:], SY[0,:]]).T
            print('  Samples used:', np.shape(S0))
            print('Ts', Ts)
            #input(' >> Press return to continue.')
            
            
        cdict = {'red':((0.0, 0.0, 0.0),
                       (2./6., 1.0, 1.0),
                       (3./6., 0.0, 0.0),
                       (4./6., 1.0, 1.0),
                       (1.0, 1.0, 1.0)),
    
             'green': ((0.0,   0.0, 0.0),
                       (2./6., 1.0, 1.0),
                       (3./6., 1.0, 1.0),
                       (4./6., 1.0, 1.0),
                       (1.0,   0.0, 0.0)),
    
             'blue':  ((0.0,   1.0, 1.0),
                       (2./6., 1.0, 1.0),
                       (3./6., 0.0, 0.0),
                       (4./6., 1.0, 1.0),
                       (1.0,   0.0, 0.0))
            }
    
        import seaborn as sns
        sns.set_style("darkgrid")
        
        from matplotlib.colors import LinearSegmentedColormap
        hyperCmap = LinearSegmentedColormap('HyperCmap', cdict)
        plt.register_cmap(cmap=hyperCmap)
       
    
        def adjustDet(D, Ts):
            r, s = np.shape(D)
            for i in range(r):
                for j in range(s):
                    if D[i,j] < -Ts: D[i,j] = - Ts
                    if D[i,j] > 2.0*Ts: D[i,j] = 2.0*Ts
                    if D[i,j] < 0.0:
                        D[i,j] = -np.log(1.0 - D[i,j])
                    if D[i,j] > Ts:
                        D[i,j] = Ts + np.log(1.0 - Ts + D[i,j])
            #D[MSK] = np.NaN
            return D
            
        print(DET.max(), DET.min(), Ts)
        
        D = adjustDet(DET, Ts)
        sx = self.sx(t0)[:]
        sy = self.sy(t0)[:]
        import scipy.interpolate as intrp
        dd = intrp.interp2d(xh, yh, (D + Ts) / (3 * Ts), kind='linear')
        DD = np.array([dd(x, y)[0] for x,y in zip(sx, sy)])
        print(DD.min(), DD.max())
        
        sn = np.log(1/2) / np.log(1/3)
        def rescale(x):
            y = np.abs(x - 0.5) * 2
            y = y ** sn
            return y
        CCC = rescale((D + Ts) / (3 * Ts))
        print(CCC.min(), CCC.max())
        ccc = intrp.interp2d(xh, yh, CCC, kind='linear')
        HHH = np.array([ccc(x, y)[0] for x,y in zip(sx, sy)])
        
        #print('>>> percentage in mesoelliptic:', np.sum(DD[np.logical_and(DD>1/3, DD<2/3)])/np.size(DD))
        c1 = 100*np.sum(DD<=1/3)/np.size(DD)
        c2 = 100*np.sum(np.logical_and(DD>1/3, DD<2/3))/np.size(DD)
        c3 = 100*np.sum(DD>=2/3)/np.size(DD)
        
        print('Blue {:.1f}%'.format(c1))
        print('Green {:.1f}%'.format(c2))
        print('Red {:.1f}%'.format(c3))
        
        fig = plt.figure(figsize=(12, 12.*13/18), dpi=200)
        ax = plt.gca()
        plt.subplots_adjust(left=0.04, right=0.98, top=0.97, bottom=0.03)
        plt.title('Time: %s' % time_to_str(t0))
        
        """
        contour_plot = ax.contourf(xh, yh, DET, 
                                    np.linspace(-Ts,2*Ts, 500), # [-1, -0.1, 0, 0.1],
                                    #alpha=0.1,
                                    #cmap=plt.cm.bone,
                                    cmap=hyperCmap,
                                    #origin=origin,
                                    zorder=0,
                                    )
        cbaxes = fig.add_axes([0.4, 0.1, 0.55, 0.02]) 
        cbar = plt.colorbar(contour_plot, ax=ax, cax = cbaxes, 
                            #fraction=0.1, shrink=0.9, pad=.01, aspect=50,
                            orientation="horizontal",
                            ticks=[-0.5*Ts, 0.5*Ts, 1.5*Ts],
                            )
        cbar.set_ticklabels(['Mesohyperbolic\n{:.1f}%'.format(c1),
                             'Mesoelliptic\n{:.1f}%'.format(c2),
                             'Mesohyperbolic\n{:.1f}%'.format(c3)])
        """
        
        contour_plot = ax.contourf(xh, yh, CCC, 
                                    np.linspace(0, 1, 500), # [-1, -0.1, 0, 0.1],
                                    #alpha=0.1,
                                    #cmap=plt.cm.bone,
                                    cmap=plt.cm.seismic,
                                    #origin=origin,
                                    zorder=0,
                                    )
        cbaxes = fig.add_axes([0.4, 0.1, 0.55, 0.012]) 
        cbar = plt.colorbar(contour_plot, ax=ax, cax = cbaxes, 
                            #fraction=0.1, shrink=0.9, pad=.01, aspect=50,
                            orientation="horizontal",
                            ticks=[0.2, 0.8],
                            )
        cbar.set_ticklabels(['Mesoelliptic\n{:.1f}%'.format(c2),
                             'Mesohyperbolic\n{:.1f}%'.format(c1+c3)])
        cbar.ax.tick_params(labelsize=14)
        
        
        
        contour_plot = ax.contourf(xh, yh, 0.2*np.ones([ny+1, nx+1]), 
                                    np.linspace(0,1), # [-1, -0.1, 0, 0.1],
                                    alpha=0.9,
                                    cmap=plt.cm.Greys,
                                    #origin=origin,
                                    zorder=10,
                                    )
        #cbar.set_clim(vmin=-Ts,vmax=2*Ts)
        
        #plt.scatter(self.sx(t0)[:], self.sy(t0)[:], c='k', s=1, alpha=0.05) 
        #contour_plot.set_clim(vmin=-Ts,vmax=2*Ts)
        
        print(sx.shape)
        
        """
        c = np.array([hyperCmap(ddd) for ddd in DD])
        ax.scatter(sx, sy, c=c, 
                    s=0.1,
                    alpha=0.05,
                    zorder=20,)
        """
        
        c = np.array([plt.cm.seismic(hhh) for hhh in HHH])
        ax.scatter(sx, sy, c=c, 
                    s=0.2,
                    alpha=0.1,
                    zorder=20,)
        
        #plt.scatter(self.tx(t0)[:], self.ty(t0)[:], c='r', s=1, alpha=0.8) 

        
        self.format_axis(ax)
        plt.savefig(self.config['results_dir'] + '/hypergraph.png')
        plt.close()
        
        """
        fig = plt.figure(figsize=(6,6))
        plt.pcolor(xh, yh, D, cmap=hyperCmap, vmin=-Ts, vmax=2*Ts)
        plt.axis('image')
        plt.colorbar(ticks=np.linspace(-Ts, 2*Ts, 4), shrink=0.6, pad = 0.05)
        plt.savefig(self.config['results_dir'] + '/hypergraph.png')
        plt.close()
        
        plt.figure(figsize=(10,10))
        plt.contourf(xh,yh, mVx)
        plt.figure(figsize=(10,10))
        plt.contourf(xh,yh, mVy)
        plt.figure(figsize=(10,10))
        plt.plot(self.SX[:,0,0],self.SY[:,0,0])
        plt.plot(self.SX[:,0,0],self.SY[:,0,0], 'k.')
        plt.plot(self.SX[0,0,0],self.SY[0,0,0], 'go')
        plt.plot(self.SX[-1,0,0],self.SY[-1,0,0], 'ro')
        """
        
        
