# -*- coding: utf-8 -*-
"""
Created on Wed Oct  8 10:27:35 2014

@author: stefan
"""

from local_paths import *
from DSMC import time_to_str
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
from scipy.stats import norm
import os


results_directory = area_p_results_dir

plot_filename = area_p_results_dir + '/area_p_comparison.pdf'

names = ['mrc.0_dsmc_', 'mrc.5_dsmc_', 'mrc.10_dsmc_']
labels = ['Search started at splash day', 'Search started 5 days after the splash', 'Search started 10 days after the splash']

import seaborn as sns
current_palette = sns.color_palette()
sns.set_style("darkgrid")

#sns.palplot(current_palette)
#print(current_palette)
#colors = ['orange', 'green', 'blue']
colors = current_palette
use_extracted_data = True
#output_time_step = 1.0/24/2
bin_width = 1.0





fig = plt.figure(figsize=(12, 4.3), dpi=200)
gs = gridspec.GridSpec(2, 1,
                       width_ratios=[1],
                       height_ratios=[2, 2]
                       )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
#ax2.set_ylim(0, 0.2)
#ax1 = fig.gca()
AT0 = []

#print sns.style()
for case, (fname, lbl, clr) in enumerate(zip(names, labels, colors)):

    AT = None
    for run in range(100):
        resfilename = '%s/%srun_%03d/convergence.npz' % (results_directory, fname, run)
        if not os.path.exists(resfilename):
            print(f'No results file: {resfilename}')
            continue
        
        npz = np.load(resfilename)
        if AT is None:
            AT = np.array([npz['detection_history']])
            time = npz['search_time']
        else:
            AT = np.append(AT, [npz['detection_history']], axis=0)
        npz.close()
        AT[-1,:][AT[-1,:]==0] = np.nan
        
        
    if AT is None:
        continue
        
    dt_avg = np.average(AT,0)
    dt_dev = np.std(AT, 0)
    dt_final = AT[:, -1]
    #time = np.array(range(len(dt_avg)))* output_time_step
    
    xx = np.linspace(np.floor(np.min(dt_final))-1, np.ceil(np.max(dt_final))+1, 200)
    #xx = np.linspace(np.floor(np.min(dt_final)/bin_width)*bin_width, np.ceil(np.max(dt_final)/bin_width)*bin_width, 200)
    mu, sigma = norm.fit(dt_final)
    pdf_fitted = norm.pdf(xx, mu, sigma)
    
    
    print('Number of runs: %d' % np.shape(AT)[0]) 
    print('mu:             %.2f' % mu)
    print('sigma:          %.2f' % sigma)
    
    #dt_max = np.ceil(np.max(dt_final / 10.0) + 0.5) * 10.0
    
    dt_min = np.min(AT,0)
    dt_max = np.max(AT,0)
    
    W = 0.2
    X = np.array([-0.1, -0.25, 0.05])+0.6 #[-0.375, -0.125, 0.125]


    #ax1.plot(time, AT.T, alpha = 0.5, c = 'black', lw=0.5, ls='-')
    #ax1.fill_between(time, np.min(AT, 0), np.max(AT,0), color=clr, alpha=0.2)    
    #ax1.plot(time, dt_avg, c = clr, lw = 2, label=lbl)    
    
    TM = (7 + np.arange(10)) * 24 + 17 + case * 5 * 24
    IT = np.array([np.argmin(np.abs(time - tm)) for tm in TM])
    ax1.bar(TM+7, dt_avg[IT], 5,
            color = clr, label=lbl, alpha=0.5,
            yerr=(dt_avg[IT]-dt_min[IT], dt_max[IT]-dt_avg[IT]), 
            error_kw=dict(ecolor='gray', lw=1, capsize=2, capthick=0.5, zorder=100))
    print(time)
    print(TM)
    print(dt_avg[IT])
    
    if case == 0:
        AT0 = dt_avg 
        time0 = time
        #AT0 = np.reshape(AT0, [np.size(AT0), 1])
        #ax1.plot(time, AT.T, alpha = 0.5, c = 'black', lw=0.5, ls='-')
        #ax1.fill_between(time, np.min(AT, 0), np.max(AT,0), color=clr, alpha=0.2)    
        #ax1.plot(time, dt_avg, c = clr, lw = 2, label=lbl)   
        
    else:
        #ax2.plot(time, dt_avg[-482:] - AT0, c = clr, lw = 3, label=lbl)
        IT = np.where((time-17.5)%24==0)[0] # pick indices at 17:30
        
        rects = ax2.bar(np.arange(10) + X[case], 
                        dt_avg[IT] - AT0[IT],
                        color=clr, width=W, label=lbl, alpha=0.5, yerr=dt_dev[IT], ecolor=clr)
        
        for ibar, rect in enumerate(rects):
            val = (dt_avg[IT] - AT0[IT])[ibar]
            #print(val)
            lx = rect.get_x()-0.1 if case == 1 else rect.get_x() + W + 0.1
            ly = val + 0.2 if val >= 0 else val - 1.2
            ax2.text(lx, ly,
                    '%1.2f' % val,
                    ha='center', va='bottom', fontsize=8)
        
    
#print(time_to_str(time0[IT[0]]))

#plt.figtext(0.003, 0.95, '(A)', fontsize=16)
#plt.figtext(0.003, 0.44, '(B)', fontsize=16)
ax1.text(0.0, 1.0, 'A', 
         weight = 'bold', fontsize=14,
         horizontalalignment='left',
         verticalalignment='bottom',
         transform=ax1.transAxes)
ax2.text(0.0, 1.0, 'B', 
         weight = 'bold', fontsize=14,
         horizontalalignment='left',
         verticalalignment='bottom',
         transform=ax2.transAxes)


#ax1.axvline(time0[IT[0]], color='k', ls='--')
#ax1.grid()
ax1.set_xlim(7*24-12, 27*24+12)
ax1.set_ylim(0, 100)
leg = ax1.legend(loc='best', fontsize=8)
leg.get_frame().set_linewidth(0)
leg.get_frame().set_alpha(0.8)
ax1.set_ylabel('Target detection\nrate [%]')
#ax1.set_xticklabels([])
x_time = np.arange(8, 28)*24.0
time_labels = [time_to_str(x-24)[:-5] for x in x_time]
ax1.xaxis.set_ticks(x_time)
ax1.xaxis.set_ticklabels(time_labels, rotation=45, fontsize=8, ha='right', rotation_mode='anchor')



ax2.axhline(0, c=colors[0], label=labels[0])
#ax2.set_xlim(time[0], time[-1])
ax2.set_ylim(-3, 5)
ax2.set_xlabel('Days of the search')
ax2.set_ylabel('Target detection\nrate difference [%]')
leg = ax2.legend(loc='best', fontsize=8)
leg.get_frame().set_linewidth(0)
leg.get_frame().set_alpha(0.8)
ax2.set_xticks(np.arange(0.5, 10))
ax2.set_xticklabels(['%d'%(i+1) for i in range(10)])

plt.subplots_adjust(left=0.065, bottom=0.103, right=0.995, top=0.955, wspace=0.05, hspace=0.6)
plt.savefig(plot_filename)




