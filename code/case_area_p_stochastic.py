#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  7 14:23:19 2018

@author: stefan
"""

from DSMC import time_to_str
from DSMC import DSMC
from local_paths import *
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import copy as cp
from scipy.stats import norm

# MRC
config = {}
config['velocity_nc_files'] = area_p_velocity_nc_files
config['results_dir'] = area_p_stoch_results_dir
config['run'] = 0
config['case'] = 'Area P'

config['vel_data_trim'] = [7, 31, 87, 105, -40, -25] # days, days, lon, lon, lat lat
config['samples_bounding_polygon'] = np.loadtxt('area_p_splash_polygon.txt')
#config['number_of_samples'] = 100000
config['samples_density'] = 2 #samples per km^2

config['advection_time'] = np.arange(7*24+0.5, 31.0*24+1e-3, 1.0) # Hours
config['number_of_targets'] = 1000

config['X'] = np.linspace(88, 104, 321)
config['Y'] = np.linspace(-39, -26, 261)


config['agents_velocity'] = 105.0 # m/s
config['detection_radius'] = 1.5 # km
config['detection_time'] = 2.0 # seconds
config['smoothing_exponent'] = 0.5
config['sigma'] = 0.03

config['search_method'] = 'mdsmc'
#t0 = 168.5
d1, d2 = 7, 17
config['search_time'] = [d1*24 + 14, d2*24-6, 60, 10]
# t1 [hours], t2 [hours], time step [seconds integer!], saving after how many time steps
config['search_regime'] =[[day*24*60 + 14*60, day*24*60 + 17*60, 10] for day in range(d1, d2)]
sig = 0.5
mrc = 0
config['uncertainty_sigma'] = sig
config['label'] = 'mrc%d_s%f_' % (mrc, sig)
runs = 100

if __name__ == '__main__':
    print('MRC')
    
    m = 'x'
    while m not in '0.1 0.2 0.5 1.0 0.1f 0.2f 0.5f 1.0f v'.split():
        m = input('Select options:\n 0.1/0.2/0.5/1.0 - drift uncertanty\n f - full run\n v - make figures\n[0.1/0.2/0.5/1.0/0.1f/0.2f/0.5f/1.0f/v]: ')
        
        
    if m[0] == 'v':
        
        t1 = (d1 + 9) * 24 + 17
        config['run'] = -1
        #run_dir = config['results_dir'] + '/%s%s_run_%03d/convergence.npz' % (config['label'], config['search_method'], config['run'])
        run_dir = config['results_dir'] + '/%s%s_run_%s/convergence.npz' % (config['label'], config['search_method'], 'full')
        config['search_time'][3] = 10
        
        
        import seaborn as sns
        sns.set_style("darkgrid")
        current_palette = sns.color_palette()
        
        """
        fig, ax = plt.subplots(figsize=(6, 4.8), dpi=200)
        
        
        dsmc = DSMC()
        dsmc.init_case(config)
        dsmc.velocity_initialization()
        dsmc.samples_advection()
        dsmc.targets_advection()
         
        dsmc.plot_samples(ax, t1, ms=0.8, alpha=0.01)
        dsmc.plot_agents(ax, t1, ms=0.5)
        dsmc.plot_targets(ax, t1)

        
        dsmc.format_axis(ax)
        # ax.text(0, 1.005, 'Some label, FIX IT!', 
        #         fontsize=14,
        #         fontweight = 'bold',
        #         horizontalalignment='left',
        #         verticalalignment='bottom', 
        #         transform=ax.transAxes)
        
        ax.set_title(f'The search accomplishment on {time_to_str(t1)}', fontsize=10) 
        plt.subplots_adjust(left=0.08, bottom=0.032, right=0.97, top=0.975)

        plot_filename = config['results_dir'] + config['case'].lower().replace(' ', '_') + '_stoch_drift_search.png'
        print(plot_filename)
        plt.savefig(plot_filename)
        """
        #input(' >> Press return to continue.')
        
        
        
        colors = current_palette

        fig = plt.figure(figsize=(12, 3), dpi=200)
        gs = gridspec.GridSpec(1, 2,
                               width_ratios=[1,1]
                               )
        ax1 = plt.subplot(gs[0])
        ax2 = plt.subplot(gs[1])
        bin_width = 1
        
        for isig, sig in enumerate([0.0, 0.1, 0.2, 0.5]):
            AT = []
            time = []
            for run in range(runs):
                if sig > 0:
                    npz_filename = f'{area_p_stoch_results_dir}/mrc{mrc}_s{sig:f}_mdsmc_run_{run:03d}/convergence.npz'
                else:
                    npz_filename = f'{area_p_results_dir}/mrc.{mrc}_dsmc_run_{run:03d}/convergence.npz'
                #print(npz_filename)
                if os.path.exists(npz_filename):
                    npz = np.load(npz_filename)
                    #print(npz.files)
                    AT.append(npz['detection_history'])
                    time = npz['search_time'] /24
            if len(time) == 0:
                continue
            AT = np.array(AT)
            #print(AT.shape)
            #input(' >> Press return to continue.')
        
            
            """
            Left plot
            """
            dt_avg = np.average(AT,0)
            dt_final = AT[:, -1]
            print(dt_final.min(), dt_final.max())
            xx = np.linspace(np.floor(np.min(dt_final))-2, np.ceil(np.max(dt_final))+2, 200)
            tm = np.append(time, time[-1]+1)
            det = np.append(dt_avg, dt_avg[-1])
            at_min = np.append(np.min(AT, 0), np.min(AT, 0)[-1])
            at_max = np.append(np.max(AT, 0), np.max(AT, 0)[-1])
            #ax1.plot(tm, det, c = clr, lw = 1.2, label=lbl)
            #ax1.fill_between(tm, at_min, at_max, color=clr, alpha=0.2)  
            
            i1 = np.argmin(np.abs(np.floor(np.min(tm)) + 17/24 - tm))
            i2 = np.argmin(np.abs(np.floor(np.min(tm)) + 1 + 17/24 - tm))
            di = i2 - i1
            IX = np.arange(i1, np.size(tm), di, dtype=int)
            
            DYS = np.arange(np.size(IX))
            # _tm = tm[IX] + (imeth - 2) / 8
            xt = DYS + isig * 0.2 - 0.3 
            
            if sig > 0:
                lbl = r'Stochastic target drift, $v_{err}=%.1f$ m/s' % sig
            else:
                lbl = 'Deterministic target drift, $v_{err}=0$ m/s'
            
            ax1.bar(xt, det[IX], width=0.15, 
                    color=colors[isig], alpha=0.5, label=lbl,
                    yerr=(det[IX]-at_min[IX], at_max[IX]-det[IX]), 
                    error_kw=dict(ecolor='gray', lw=1, capsize=2, capthick=0.5))
            
            
            """
            Right plot
            """
            mu, sigma = norm.fit(dt_final)
            pdf_fitted = norm.pdf(xx, mu, sigma)
            boxtxt = '$\sigma=%.2f$, $\mu=%.2f$' % (sigma, mu)
            pdf, bins, patches = ax2.hist(AT[:,-1], np.arange(xx[0], xx[-1]+1e-10, bin_width), alpha=0.5, color=colors[isig], 
                                          weights=np.ones_like(AT[:,-1])/np.shape(AT)[0], density=False, histtype='bar', label=lbl + ' (' + boxtxt + ')')
            print('bins sum:', np.sum(pdf))
            #ax2.plot([np.average(dt_final), np.average(dt_final)], [0, 0.3], 'b-', lw=2, label='Average')
            ax2.axvline(np.average(dt_final), c=colors[isig], ls='-', lw=0.5)
            #ax2.axvline(np.median(dt_final), c=clr, ls='--', lw=2)
            ax2.fill_between(xx, pdf_fitted, ls='-', color=colors[isig], alpha=0.2, lw=0.5)
            
        ax1.set_xlabel(f'Days of search starting from 2014-03-08', fontsize=8)
        ax1.set_ylabel('Targets detection rate [%]', fontsize=8)
        ax1.set_xticks(DYS)
        ax1.set_xticklabels(np.arange(len(DYS)) + 1,
                            rotation=0, fontsize=8)
        ax1.set_xlim(DYS[0] - 0.5, DYS[-1] + 0.5)        
        ax1.set_ylim(0, 100)
        
        leg = ax1.legend(loc='best', fontsize=8)
        leg.get_frame().set_linewidth(0)
        leg.get_frame().set_alpha(0.8)
        ax1.tick_params(axis='both', which='major', labelsize=8)
        #ax2.grid()
        ax2.set_xlabel('Targets detection rate [%]', fontsize=8)
        ax2.set_ylabel('Frequency [-]', fontsize=8)
        ax2.set_xlim(0, 100)
        ax2.set_ylim(0, ax2.get_ylim()[1]*1.3)
        leg = ax2.legend(loc='best', fontsize=8)
        leg.get_frame().set_linewidth(0)
        leg.get_frame().set_alpha(0.8)
        ax2.tick_params(axis='both', which='major', labelsize=8)
        
        
        ax1.text(0.0, 1.0, 'A', 
                 weight = 'bold', fontsize=14,
                 horizontalalignment='left',
                 verticalalignment='bottom',
                 transform=ax1.transAxes)
        ax2.text(0.0, 1.0, 'B', 
                 weight = 'bold', fontsize=14,
                 horizontalalignment='left',
                 verticalalignment='bottom',
                 transform=ax2.transAxes)
        
        plt.subplots_adjust(left=0.045, bottom=0.13, right=0.99, top=0.93, wspace=0.15)
        # if case == 'Area A':
        #     plt.subplots_adjust(left=0.045, bottom=0.13, right=0.99, top=0.93, wspace=0.15)
        # if case == 'Area B':
        #     plt.subplots_adjust(left=0.045, bottom=0.17, right=0.99, top=0.91, wspace=0.15)
            
        plot_filename = 'area_p_stoch_drift_comparison.pdf'
        plt.savefig(area_p_stoch_results_dir + '/' + plot_filename)
        
    else:
        full_run = m[-1] == 'f'
        runs = 1 if full_run else runs
        
        
        sig = float(m[:-1]) if full_run else float(m)
        config['uncertainty_sigma'] = sig
        config['label'] = 'mrc%d_s%f_' % (mrc, sig)
            
        # Start an end day of the search
        d1 = 7 + mrc
        d2 = d1 + 10
        
        config['search_time'] = [d1*24 + 14, d2*24-6, 60, 1*60*24] # t1 [hours], t2 [hours], time step [seconds integer!], saving after how many time steps
        # start [minutes], end [minutes], number_of_agents
        config['search_regime'] =[[day*24*60 + 14*60, day*24*60 + 17*60, 10] for day in range(d1, d2)]
    
            
        for r in range(runs):
            
            if full_run:
                config['run'] = -1
                run_dir = config['results_dir'] + '/%s%s_run_%s/convergence.npz' % (config['label'], config['search_method'], 'full')
                config['search_time'][3] = 10
            else:
                config['run'] = r
                run_dir = config['results_dir'] + '/%s%s_run_%03d/convergence.npz' % (config['label'], config['search_method'], config['run'])
                config['search_time'][3] = 60
                
            print(run_dir)
            if os.path.exists(run_dir):
                print('exists!')
                continue
            #input()
    
            dsmc = DSMC()     
            dsmc.detection_substeps = 25
            dsmc.init_case(config)
            dsmc.velocity_initialization()
            dsmc.samples_advection()
            dsmc.targets_advection()
            dsmc.run_search()
            del dsmc