#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  3 13:58:44 2018

@author: stefan
"""

import numpy as np
import os
import scipy.interpolate as sp_intrp
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
from local_paths import *
from shapely.geometry import Polygon, Point, LineString
from descartes.patch import PolygonPatch
            


import datetime
time0 = datetime.datetime(2014, 3, 1, 0, 0)
def time_to_str(time):    
    time = time0 + datetime.timedelta(hours=time)
    return time.strftime('%Y-%m-%d %H:%M')

import types
import functools

def copy_func(f):
    """Based on http://stackoverflow.com/a/6528148/190597 (Glenn Maynard)"""
    g = types.FunctionType(f.__code__, f.__globals__, name=f.__name__,
                           argdefs=f.__defaults__,
                           closure=f.__closure__)
    g = functools.update_wrapper(g, f)
    g.__kwdefaults__ = f.__kwdefaults__
    return g

class MH370_animation():
    
    def __init__(self, case):
        self.case = case
        self.methods = ['lwnmwrpredef', 'lwnmwrhull', 'mdsmc']
        #self.methods = ['lwnmwrpredef']
        
        if case == 'area_a':
            self.results_dir = area_a_results_dir
            self.X = np.linspace(92, 101, 361)
            self.Y = np.linspace(-35, -27, 321)
            t1, t2, dt, snt = [27*24 + 0.5, 31*24+10, 10, 3]
            
        if case == 'area_b':
            self.results_dir = area_b_results_dir
            self.X = np.linspace(92, 106, 561)
            self.Y = np.linspace(-33, -26, 281)
            t1, t2, dt, snt = [32*24 + 0.5, 33*24+10, 10, 3]
            
        nt = int(np.round((t2 - t1) * 3600 / dt) + 1)
        self.search_time = np.linspace(t1*3600, t2 * 3600, nt) / 3600.0 # [hours]
        self.SIT = np.arange(0, nt, snt)
        self.search_step = dt / 3600.0 # [hours]
        
        if not os.path.exists(self.results_dir + 'animation'):
            os.makedirs(self.results_dir + 'animation')
        
        self.init_samples()
        self.init_targets()
            
        self.search_status = None
    
    def init_samples(self):
        
        print('Reading samples advection from', self.results_dir + 'advection.npz')
        npz = np.load(self.results_dir + 'advection.npz')
        self.ST = npz['st']
        self.SX = npz['sx']
        self.SY = npz['sy']
        self.S0 = np.vstack([self.SX[0,:], self.SY[0,:]]).T
        print('  Done')
         
        print('Advected samples bounds: [%f, %f] x [%f, %f]' % (np.min(self.SX), np.max(self.SX), np.min(self.SY), np.max(self.SY)))
        #input(' > Press return to continue')
        
        def sx(t):
            it2 = np.argmax(self.ST > t)
            #it2 = np.argmin(np.abs(self.ST - t))
            #it2 = np.min(np.where(self.ST >= t))
            if self.ST[it2] < t:
                it2 = it2 + 1 
            it1 = it2 - 1
            
            w1 = (self.ST[it2] - t) / (self.ST[it2] - self.ST[it1])
            w2 = (t - self.ST[it1]) / (self.ST[it2] - self.ST[it1])
            
            sx = self.SX[it1,:] * w1 + self.SX[it2,:] * w2    
            return sx
        
        def sy(t):
            it2 = np.argmax(self.ST > t)
            #it2 = np.argmin(np.abs(self.ST - t))
            #it2 = np.min(np.where(self.ST >= t))
            if self.ST[it2] < t:
                it2 = it2 + 1 
            it1 = it2 - 1
            
            w1 = (self.ST[it2] - t) / (self.ST[it2] - self.ST[it1])
            w2 = (t - self.ST[it1]) / (self.ST[it2] - self.ST[it1])
            
            sy = self.SY[it1,:] * w1 + self.SY[it2,:] * w2    
            return sy
        
        def mu(t, xh, yh):
            it2 = np.argmax(self.ST > t)
            #it2 = np.argmin(np.abs(self.ST - t))
            #it2 = np.min(np.where(self.ST >= t))
            if self.ST[it2] < t:
                it2 = it2 + 1 
            it1 = it2 - 1
            
            w1 = (self.ST[it2] - t) / (self.ST[it2] - self.ST[it1])
            w2 = (t - self.ST[it1]) / (self.ST[it2] - self.ST[it1])
            
            sx = self.SX[it1,:] * w1 + self.SX[it2,:] * w2
            sy = self.SY[it1,:] * w1 + self.SY[it2,:] * w2
            
            m, _, _ = np.histogram2d(sy, sx, bins=[yh, xh])
            return m
        
        
        self.sx_interp = sp_intrp.interp1d(self.ST, self.SX[:,0])
        self.sx = sx
        self.sy = sy
        self.mu = mu
    

    def init_targets(self):
        
        def generate_interpolation_functions(TT, TX, TY):
            def tx_fun(t):
                it2 = np.argmax(TT > t)
                #it2 = np.argmin(np.abs(self.ST - t))
                #it2 = np.min(np.where(self.ST >= t))
                if TT[it2] < t:
                    it2 = it2 + 1 
                it1 = it2 - 1
                
                w1 = (TT[it2] - t) / (TT[it2] - TT[it1])
                w2 = (t - TT[it1]) / (TT[it2] - TT[it1])
                
                tx = TX[it1,:] * w1 + TX[it2,:] * w2    
                return tx
            
            def ty_fun(t):
                it2 = np.argmax(TT > t)
                #it2 = np.argmin(np.abs(self.ST - t))
                if TT[it2] < t:
                    it2 = it2 + 1 
                it1 = it2 - 1
                w1 = (t - TT[it1]) / (TT[it2] - TT[it1])
                w2 = (TT[it2] - t) / (TT[it2] - TT[it1])
                
                ty = TY[it1,:] * w1 + TY[it2,:] * w2    
                return ty
            
            return tx_fun, ty_fun
            
        self.tx, self.ty = {}, {}
        for method in self.methods:
    
            print('Reading target advection from', '%s%s_run_full/targets.npz' % (self.results_dir, method))
            npz = np.load('%s%s_run_full/targets.npz' % (self.results_dir, method))
            TT = npz['st']
            TX = npz['sx']
            TY = npz['sy']
            #self.T0 = np.vstack([self.TX[0,:], self.TY[0,:]]).T
            #self.number_of_targets = np.shape(self.TX)[1]
            npz.close()
            print('  Done')
            #print(self.TT[0], self.TX[0,0], self.TY[0,0])
            #input(' >> Press return to continue.')
            print('TT [%f ... %f]' % (TT.min(), TT.max()))
            print('TX [%f ... %f]' % (TX.min(), TX.max()))
            print('TY [%f ... %f]' % (TY.min(), TY.max()))
                        
            #self.tx[method] = tx
            #self.ty[method] = ty
            self.tx[method], self.ty[method] = generate_interpolation_functions(TT, TX, TY)
            
            
        #print(self.tx, self.ty)
        """
        print('tx/ty min/max @t=373',
              self.tx[method](737).min(),
              self.tx[method](737).max(), 
              self.ty[method](737).min(), 
              self.ty[method](737).max())
        """
        
    def plot_lwnmower_areas(self, ax):
        
        f = open(self.case + '_lawnmower.txt')
        lines = f.readlines()
        f.close()
        
        DATES = []
        XY = []
        for line in lines:
            if line[0] == '#':
                DATES.append(line.strip())
            else:
                XY.append([float(s) for s in line.split()])
        
        for i in range(0, len(XY), 2):
            SP = np.array([XY[i], XY[i+1]]).T
                
            polygon = Polygon(SP).convex_hull
            patch1 = PolygonPatch(polygon, facecolor='purple', edgecolor=None, alpha=0.1, zorder=2)
            patch2 = PolygonPatch(polygon, fill=None, edgecolor='black', alpha=0.1, zorder=3, linewidth=1)
            ax.add_patch(patch1)
            ax.add_patch(patch2)
            
        return (patch1,patch2)
        
    def plot_search(self, ax, method, t, legend=False, only_advection=False, summary_plot=False):
        #print(method)
        it = np.argmin(np.abs(self.search_time - t))
        sit = np.argmin(np.abs(self.SIT - it))
               
        # Samples
        alpha = 0.2 if method == 'dsmc' else 0.05
        alpha = 0.3 if summary_plot else alpha
        samples = ax.scatter(self.sx(t), self.sy(t), s=0.01, c='grey', alpha=alpha)
                
        
        # Agents
        if not only_advection:
            npz_filename = '%s%s_run_full/iterations/search_%010d.npz' % (self.results_dir, method, sit)
            print(npz_filename)
            npz = np.load(npz_filename)
            detected = npz['detected']
            undetected = npz['undetected']
            AAX = npz['AAX']
            AAY = npz['AAY']
            AX = npz['AX']
            AY = npz['AY']
            
            #print(AX.shape)
            
            ms = 0.08 if method == 'dsmc' else 0.02
            alpha = 1 if method == 'dsmc' else 0.6
            
            ms, alpha = (0.1, 0.3) if summary_plot else (ms, alpha)
            
            agents = ax.scatter(AAX, AAY, s=ms, c='purple', alpha=alpha)
            if self.search_status == 'search':
                ax.scatter(AX, AY, s=ms*400, c='purple', zorder=200)
        
        
        
        # Targets
        ms = 2 if method == 'dsmc' else .5
        ms = 1 if summary_plot else ms
        
        if only_advection:
            targets_u = ax.scatter(self.tx[method](t), self.ty[method](t), s=ms, c='red', alpha=0.8)
        else:
            targets_u = ax.scatter(self.tx[method](t)[undetected], self.ty[method](t)[undetected], s=ms, c='red', alpha=0.8)
            targets_d = ax.scatter(self.tx[method](t)[detected], self.ty[method](t)[detected], s=ms*8, c='green', alpha=1.0, marker='x')
        
        
        if legend:
            plt.legend([Patch(facecolor='lightgrey', edgecolor=None, alpha=0.5), 
                        Line2D([0], [0], color='purple', lw=0.5),
                        targets_u, targets_d], 
                       ['Drifted target probability distribution', 'Search agent trajectory', 'Undetected targets', 'Detected targets'],
                       scatterpoints=3,
                       loc='upper center')
        """
        print('tx/ty min/max @t=%f' % t,
              self.tx[method](t).min(),
              self.tx[method](t).max(), 
              self.ty[method](t).min(), 
              self.ty[method](t).max())
        """
        if not only_advection:
            detection_rate = np.size(detected) / 1000
            ax.text(0.99, 0.94 if summary_plot else 0.01, 'Detection rate: %.1f%%' % (100 * detection_rate),
                    verticalalignment='bottom', horizontalalignment='right',
                    transform=ax.transAxes,
                    color='k', 
                    fontsize=10
                    )
        
        self.format_axis(ax)
    
        
    def plot_attraction(self, ax, method, t):
        
        it = np.argmin(np.abs(self.search_time - t))
        sit = np.argmin(np.abs(self.SIT - it))
        
        npz_filename = '%s%s_run_full/iterations/search_%010d.npz' % (self.results_dir, method, sit)
        #print('U field:', npz_filename)
        npz = np.load(npz_filename)
        u = npz['U']
        ux = npz['Ux']
        uy = npz['Uy']
        npz.close()
        
        un = np.sqrt(ux**2 + uy**2)
        ux /= un
        uy /= un
        #print(u)
        
        ax.contourf(self.X, self.Y, u, 50, alpha=0.8, cmap=plt.cm.plasma)
        #plt.colorbar()
        """
        qn = 8
        ax.quiver(self.X[::qn], self.Y[::qn], ux[::qn,::qn], uy[::qn,::qn], 
                   color='k', alpha=0.5, scale=40, headwidth=5, headlength=7, headaxislength=7)
        """
        #plt.contourf(self.X, self.Y, self.MU, alpha=1, cmap=plt.cm.GnBu)
        
        self.format_axis(ax)
        
        
    def plot_detection_rate(self, ax, t):
        
        it = np.argmin(np.abs(self.search_time - t))
        #sit = np.argmin(np.abs(self.SIT - it))
        
        dirs = ['dsmc', 'lwnmwrpredef', 'lwnmwrhull']
        methods = ['mDSMC', 'Lawnmower scenario 1', 'Lawnmower scenario 2']
        for dirname, method in zip(dirs, methods):
            npz_filename = '%s%s_run_full/convergence.npz' % (self.results_dir, dirname)
            if not os.path.exists(npz_filename):
                continue
            npz = np.load(npz_filename)
            time = npz['search_time']
            det = npz['detection_history']
            npz.close()
            
            if dirname == 'dsmc':
                ax.plot(time[:it], det[:it], label=method, lw=1.5, ls='-')
            else:
                ax.plot(time[:it], det[:it], label=method, lw=1, ls='--')
        
        x_time = time[np.where(time%24==0)]
        x_time = np.arange(x_time[0]-24, x_time[-1]+24, 24)
        #print(time)
        #print(x_time)
        ax.xaxis.set_ticks(x_time)
        ax.xaxis.set_ticklabels([])
        
        for x in x_time:
            ax.text(x+12, -5, time_to_str(x)[:-5],
                    ha='center',
                    va='top',
                    rotation=0 if self.case == 'area_b' else 15)
        
        ax.set_xlim(x_time[0], x_time[-1]+24)
        ax.set_ylim(-3, 103)
        ax.set_ylabel('Detection rate [%]')
        ax.legend(loc='upper left')
    
    def format_axis(self, ax):
        ax.axis('image')
        ax.set_xlim(self.X[0], self.X[-1])
        ax.set_ylim(self.Y[0], self.Y[-1])
        
        xlbl = np.arange(self.X[0], self.X[-1]+1e-3)
        ylbl = np.arange(self.Y[0], self.Y[-1]+1e-3)
        ax.set_xticks(xlbl)
        ax.set_yticks(ylbl)
        ax.set_xticklabels([u'%.0f°E' % deg for deg in ax.xaxis.get_majorticklocs()])
        ax.set_yticklabels([u'%.0f°S' % -deg for deg in ax.yaxis.get_majorticklocs()])
        
        ax.tick_params(direction='out', pad=7)
        plt.draw()
        
    def plot_advection(self, ax, sit, summary_plot=False):
        
        it = self.SIT[sit]
        t = self.search_time[it]
        samples = ax.scatter(self.sx(t), self.sy(t), s=0.01, c='steelblue', alpha=0.1 if summary_plot else 0.8)
        
        if self.case == 'area_a':
            SP = np.array([
            [96.3925681965,	-33.85301067],
            [98.4352402091,	-31.82609007],
            [97.3236915587,	-29.62465096],
            [95.8137037302,	-31.34907281]
            ])
            
        if self.case == 'area_b':
            SP = np.loadtxt('area_b_splash_polygon.txt')
                
        polygon = Polygon(SP).convex_hull
        patch1 = PolygonPatch(polygon, facecolor='orange', edgecolor=None, alpha=0.3, zorder=2)
        patch2 = PolygonPatch(polygon, fill=None, edgecolor='black', alpha=0.3, zorder=3, linewidth=1)
        ax.add_patch(patch1)
        ax.add_patch(patch2)

        patch3 = self.plot_lwnmower_areas(ax)

        plt.legend([(patch1,patch2), Patch(facecolor='steelblue', edgecolor=None), patch3], 
                   ['Estimated splash area', 'Drifted target\nprobability distribution', 'Reported search areas'],
                   scatterpoints=10)
        
        # Add the legend manually to the current Axes.
        #ax = plt.gca().add_artist(first_legend)
            
        self.format_axis(ax)
        
    def make_frame(self, t, fname, only_advection=False):
        
        #it = np.argmin(np.abs(self.search_time - t))
        #sit = np.argmin(np.abs(self.SIT - it))
        
        plt.figure(figsize=(19.2, 10.8))
        
        if self.case == 'area_a':
            gs = gridspec.GridSpec(2, 4,
                                   width_ratios=[1, 1.2, 1.2, 1])
            
            ax_lwnpredef = plt.subplot(gs[0, 0])       
            ax_lwnhull = plt.subplot(gs[1, 0])
            ax_dsmc = plt.subplot(gs[:, 1:3])
            ax_att = plt.subplot(gs[0, 3])
            ax_det = plt.subplot(gs[1, 3])
            
        
        if self.case == 'area_b':
            gs = gridspec.GridSpec(3, 3,
                                   width_ratios=[1, 1, 1],
                                   height_ratios=[1, 1, 1])
            
            ax_dsmc = plt.subplot(gs[:2, :2])
            ax_lwnpredef = plt.subplot(gs[2, 0])       
            ax_lwnhull = plt.subplot(gs[2, 1])
            ax_att = plt.subplot(gs[0, 2])
            ax_det = plt.subplot(gs[2, 2])
        
        self.plot_search(ax_dsmc, self.methods[2], t, legend=False, only_advection=only_advection)
        
        if not only_advection:
            self.plot_search(ax_lwnpredef, self.methods[0], t, legend=False, only_advection=only_advection)
            self.plot_search(ax_lwnhull, self.methods[1], t, legend=False, only_advection=only_advection)
            self.plot_attraction(ax_att, self.methods[2], t)
            self.plot_detection_rate(ax_det, t)
    
        if only_advection:
            ax_lwnpredef.axis('off')
            ax_lwnhull.axis('off')
            ax_att.axis('off')
            ax_det.axis('off')
          
        if self.case == 'area_a':  
            plt.subplots_adjust(left=0.03, right=0.985,
                                bottom=0.12, top=0.88, 
                                hspace=0.4, wspace=0.12)
            
        if self.case == 'area_b':  
            plt.subplots_adjust(left=0.03, right=0.98,
                                bottom=0.09, top=0.93, 
                                hspace=0.2, wspace=0.2)
        
        if not only_advection and self.case == 'area_a':
            
            plt.figtext(0.229, 0.88, 
                        'Lawnmower scenario 1',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
        
            plt.figtext(0.229, 0.436, 
                        'Lawnmower scenario 2',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
            
            plt.figtext(0.748, 0.88, 
                        'mDSMC search',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
        
            plt.figtext(0.985, 0.88, 
                        'mDSMC attraction field',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
            
            plt.figtext(0.985, 0.436, 
                        'Detection rate',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
            
        if not only_advection and self.case == 'area_b':
            
            plt.figtext(0.309, 0.337, 
                        'Lawnmower scenario 1',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
        
            plt.figtext(0.644, 0.337, 
                        'Lawnmower scenario 2',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
            
            plt.figtext(0.644, 0.93, 
                        'mDSMC search',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
        
            plt.figtext(0.98, 0.93, 
                        'mDSMC attraction field',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
            
            plt.figtext(0.98, 0.337, 
                        'Detection rate',
                        va='bottom', ha='right',
                        #weight='bold',
                        color='k', fontsize=10)
            
            
        if self.case == 'area_a':  
            plt.figtext(0.01, 0.96, 
                        'Video 1: Simulation of area A search',
                        va='center', ha='left',
                        weight='bold',
                        color='k', fontsize=16)
            
            plt.figtext(0.5, 0.92, 
                        time_to_str(t),
                        va='center', ha='center',
                        #transform=ax_det.transAxes,
                        color='k', fontsize=16)
        
        if self.case == 'area_b':  
            plt.figtext(0.01, 0.96, 
                        'Video 2: Simulation of area B search',
                        va='center', ha='left',
                        weight='bold',
                        color='k', fontsize=16)
            
            plt.figtext(0.75, 0.55, 
                        time_to_str(t),
                        va='center', ha='center',
                        #transform=ax_det.transAxes,
                        color='k', fontsize=16)
            
        plt.figtext(0.01, 0.03, 
                    'Search strategy in a complex and dynamic environment: the MH370 case',
                    va='bottom', ha='left',
                    weight='bold',
                    color='k', fontsize=12)
        plt.figtext(0.01, 0.01, 
                    'Stefan Ivić, Bojan Crnković, Hassan Arbabi, Sophie Loire, Patrick Clary, Igor Mezić',
                    va='bottom', ha='left',
                    #transform=ax_det.transAxes,
                    color='k', fontsize=12)
        
        
        #for ax in [ax_dsmc, ax_lwnpredef, ax_lwnhull, ax_att, ax_det]:
        #    print(ax.get_position())
            
        plt.savefig(fname, dpi=100)
        plt.close()
        print(time_to_str(t), self.search_status, fname)

    def make_search_summary_plot(self):
        
        if self.case == 'area_a':
            sit1 = 660
            sit2 = 12540
        if self.case == 'area_b':
            sit1 = 660
            sit2 = 3900
        
        it1 = self.SIT[sit1]
        t1 = self.search_time[it1]
        it2 = self.SIT[sit2]
        t2 = self.search_time[it2]
        
        print('Making search summary plot for t=%f (%s)' % (t2, time_to_str(t2)))
        
        import seaborn as sns
        sns.set_style("darkgrid")

        if self.case == 'area_a':
            plt.figure(figsize=(15, 13))
            gs = gridspec.GridSpec(2, 2,
                                   left=0.03,
                                   right=0.99,
                                   bottom=0.025,
                                   top=0.98,
                                   hspace=0.1,
                                   wspace=0.1)
        if self.case == 'area_b':
            plt.figure(figsize=(15, 8))
            gs = gridspec.GridSpec(2, 2,
                                   left=0.035,
                                   right=0.985,
                                   bottom=0.02,
                                   top=0.98,
                                   hspace=0.1,
                                   wspace=0.1)
                
        ax_advection = plt.subplot(gs[0, 0])       
        self.plot_advection(ax_advection, sit1, summary_plot=True)
        ax_advection.set_title('Drifted target probability distribution on %s' % time_to_str(t1))
        ax_advection.text(0.0, 1.0, 'A', 
                          weight = 'bold', fontsize=18,
                          horizontalalignment='left',
                          verticalalignment='bottom',
                          transform=ax_advection.transAxes)
        
        ax_dsmc = plt.subplot(gs[0, 1])
        self.plot_search(ax_dsmc, self.methods[2], t2, legend=True, summary_plot=True)
        ax_dsmc.set_title('mDSMC search on %s' % time_to_str(t2))
        ax_dsmc.text(0.0, 1.0, 'B', 
                     weight = 'bold', fontsize=18,
                     horizontalalignment='left',
                     verticalalignment='bottom',
                     transform=ax_dsmc.transAxes)
        
        ax_lwnpredef = plt.subplot(gs[1, 0])
        self.plot_search(ax_lwnpredef, self.methods[0], t2, summary_plot=True)
        ax_lwnpredef.set_title('Lawnmower scenario 1 search on %s' % time_to_str(t2))
        ax_lwnpredef.text(0.0, 1.0, 'C', 
                          weight = 'bold', fontsize=18,
                          horizontalalignment='left',
                          verticalalignment='bottom',
                          transform=ax_lwnpredef.transAxes)

        
        ax_lwnhull = plt.subplot(gs[1, 1])
        self.plot_search(ax_lwnhull, self.methods[1], t2, summary_plot=True)
        ax_lwnhull.set_title('Lawnmower scenario 2 search on %s' % time_to_str(t2))
        ax_lwnhull.text(0.0, 1.0, 'D', 
                        weight = 'bold', fontsize=18,
                        horizontalalignment='left',
                        verticalalignment='bottom',
                        transform=ax_lwnhull.transAxes)
            
        plt.savefig(self.results_dir + '%s_search.png' % self.case, dpi=150)
        
        for ax in [ax_advection, ax_dsmc, ax_lwnpredef, ax_lwnhull]:
            print(ax.get_position())

        plt.close()
        
        print('Summary search figure made for %s on %s' % (self.case, time_to_str(t2)))
        
        
        
        
        
import seaborn as sns
sns.set_style("darkgrid")


animation = MH370_animation('area_b')
animation.make_search_summary_plot()

if animation.case == 'area_a':  
    days = range(27, 32)
if animation.case == 'area_b':  
    days = range(32, 34)
    
    

print('0.advection')
# 0.Advection
if not os.path.exists(animation.results_dir + 'animation/0.advection'):
    os.makedirs(animation.results_dir + 'animation/0.advection')
animation.search_status = 'advection'    
for t in np.arange(7*24+0.5, days[0]*24+6+1e-3, 1):
    fname = animation.results_dir + 'animation/0.advection/' + (time_to_str(t)+'.png').replace(':','-')
    if os.path.exists(fname):
        #pass
        continue
    animation.make_frame(t, fname, only_advection=True)
    



for iday, day in enumerate(days):
    #Search
    print('%d.search' % (iday+1))
    if not os.path.exists(animation.results_dir + 'animation/%d.search' % (iday + 1)):
        os.makedirs(animation.results_dir + 'animation/%d.search' % (iday + 1))
    animation.search_status = 'search'    
    for t in np.arange(day*24+6, day*24+9+1e-3, 1/60):
        fname = animation.results_dir + 'animation/%d.search/' % (iday + 1) + (time_to_str(t)+'.png').replace(':','-')
        if os.path.exists(fname):
            continue
        animation.make_frame(t, fname)
                
    # Advection
    print('%d.advection' % (iday+1))
    if not os.path.exists(animation.results_dir + 'animation/%d.advection' % (iday + 1)):
        os.makedirs(animation.results_dir + 'animation/%d.advection' % (iday + 1))
    animation.search_status = 'advection'    
    for t in np.arange(day*24+9, (day+1)*24+6+1e-3, 1):
        fname = animation.results_dir + 'animation/%d.advection/' % (iday + 1) + (time_to_str(t)+'.png').replace(':','-')
        if os.path.exists(fname):
            #pass
            continue
        animation.make_frame(t, fname)

t = days[0]*24+6
fname = animation.results_dir + 'animation/0.end_a.png'
animation.make_frame(t, fname, only_advection=True)
fname = animation.results_dir + 'animation/0.end_b.png'
animation.make_frame(t, fname)

        