#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  7 14:23:19 2018

@author: stefan
"""

from DSMC import DSMC
from local_paths import *
import os
import numpy as np
import matplotlib.pyplot as plt


# Case A
config = {}
config['velocity_nc_files'] = area_a_velocity_nc_files
config['results_dir'] = area_a_results_dir
config['run'] = 0
config['case'] = 'Area A'

config['vel_data_trim'] = [7, 32, 92, 101, -35, -27] # days, days, lon, lon, lat lat
config['samples_bounding_polygon'] = np.array([(96.3925681965, -33.85301067),
                                               (98.4352402091, -31.82609007),
                                               (97.3236915587, -29.62465096),
                                               (95.8137037302, -31.34907281)])
#config['number_of_samples'] = 100000
config['samples_density'] = 5 #samples per km^2

config['advection_time'] = np.arange(7*24+0.5, 32.0*24+1e-3, 1.0) # Hours
config['number_of_targets'] = 1000

config['X'] = np.linspace(92, 101, 361)
config['Y'] = np.linspace(-35, -27, 321)
config['search_time'] = [27*24 + 0.5, 31*24+10, 10, 6*30] # t1 [hours], t2 [hours], time step [seconds integer!], saving after how many time steps
config['search_regime'] = [(27*24*60 + 6*60, 27*24*60 + 9*60, 10), # start [minutes], end [minutes], number_of_agents
                           (28*24*60 + 6*60, 28*24*60 + 9*60, 8),
                           (29*24*60 + 6*60, 29*24*60 + 9*60, 10),
                           (30*24*60 + 6*60, 30*24*60 + 9*60, 10),
                           (31*24*60 + 6*60, 31*24*60 + 9*60, 11)]
config['agents_velocity'] = 105.0 # m/s
config['detection_radius'] = 1.5 # km
config['detection_time'] = 2.0 # seconds
config['sigma'] = 0.03
config['uncertainty_sigma'] = 0


if __name__ == '__main__':
    print('Area A')
    
    m = 'x'
    options = '1 2 3 4 1f 2f 3f 4f c v'
    while m not in options.split():
        m = input('Select the search method:\n 1 - DSMC\n 2 - mDSMC\n 3 - Lawnmower predefined\n 4 - Lawnmower convex hull\n f - full run\n c - generate common files\n v - generate search visualization figures\n[' + '/'.join(options.split()) + ']: ')
    
    if m[0] == 'c':
        
        config['search_method'] = 'common files'
        dsmc = DSMC()     
        dsmc.create_common_files(config)
        del dsmc
       
    elif m[0] == 'v':
        
        config['search_method'] = 'visualization'
        config['search_time'][3] = 3
        dsmc = DSMC()     
        dsmc.make_figures(config)
        del dsmc   
        
    else:
        
        if m[0] == '1':
            config['search_method'] = 'dsmc'
        elif m[0] == '2':
            config['search_method'] = 'mdsmc'
        elif m[0] == '3':
            config['search_method'] = 'lwnmwrpredef'
            config['hull_points'] = np.loadtxt('area_a_lawnmower.txt')
        elif m[0] == '4':
            config['search_method'] = 'lwnmwrhull'
            
        full_run = len(m) == 2
        
        runs = 1 if full_run else 100
        
        for r in range(runs):
            
            if full_run:
                config['run'] = -1
                run_dir = config['results_dir'] + '/%s_run_%s/convergence.npz' % (config['search_method'], 'full')
                config['search_time'][3] = 3
            else:
                config['run'] = r
                run_dir = config['results_dir'] + '/%s_run_%03d/convergence.npz' % (config['search_method'], config['run'])
            
            print(run_dir)
            if os.path.exists(run_dir):
                print('exists!')
                continue
    
            dsmc = DSMC()     
            dsmc.init_case(config)
            dsmc.velocity_initialization()
            dsmc.samples_advection()
            dsmc.targets_advection()
            dsmc.run_search()
            del dsmc
            