#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb  7 14:23:19 2018

@author: stefan
"""

from DSMC import time_to_str
from DSMC import DSMC
from local_paths import *
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import copy as cp

# MRC
config = {}
config['velocity_nc_files'] = area_p_velocity_nc_files
config['results_dir'] = area_p_results_dir
config['run'] = 0
config['case'] = 'Area P'

config['vel_data_trim'] = [7, 31, 87, 105, -40, -25] # days, days, lon, lon, lat lat
config['samples_bounding_polygon'] = np.loadtxt('area_p_splash_polygon.txt')
#config['number_of_samples'] = 100000
config['samples_density'] = 2 #samples per km^2

config['advection_time'] = np.arange(7*24+0.5, 31.0*24+1e-3, 1.0) # Hours
config['number_of_targets'] = 1000

config['X'] = np.linspace(88, 104, 321)
config['Y'] = np.linspace(-39, -26, 261)


config['agents_velocity'] = 105.0 # m/s
config['detection_radius'] = 1.5 # km
config['detection_time'] = 2.0 # seconds
config['smoothing_exponent'] = 0.5
config['sigma'] = 0.03
config['uncertainty_sigma'] = 0

config['search_method'] = 'mdsmc'
#t0 = 168.5
d1, d2 = 17, 27
config['search_time'] = [d1*24 + 6, d2*24-6, 60, 10]
# t1 [hours], t2 [hours], time step [seconds integer!], saving after how many time steps
config['search_regime'] =[[day*24*60 + 6*60, day*24*60 + 9*60, 10] for day in range(d1, d2)]

if __name__ == '__main__':
    print('MRC')
    
    m = 'x'
    while m not in '0 5 10 0f 5f 10f v'.split():
        m = input('Select options:\n 0/5/10 - search start day\n f - full run\n v - make figures\n[0/5/10/0f/5f/10f/v]: ')
        
        
    if m[0] == 'v':
        
        full_run = True
        config['run'] = -1 # It's a full run
        config['search_regime'] =[[day*24*60 + 6*60, day*24*60 + 9*60, 10] for day in range(17, 27)]
        config['label'] = 'mrc.%d_s0.1_' % (10)
        dsmc = DSMC()
        
        dsmc.config = cp.deepcopy(config)
        dsmc.init_case(config)   

    
        import seaborn as sns
        sns.set_style("darkgrid")
        plt.figure(figsize=(12, 9.6), dpi=200)
        gs = gridspec.GridSpec(2, 2, width_ratios=[1,1], height_ratios=[1,1])
        plt.subplots_adjust(left=0.05, bottom=0.03, right=0.98, top=0.975, wspace=0.15, hspace=0.15)

        labels = 'ABCD'
        MRC = [None, 0, 5, 10]
        T0 = np.array([np.nan, 7, 12, 17]) * 24. + 6
        #T1 = np.array([12, 17, 22, 27]) * 24. + 17 - 24
        T1 = np.array([27, 7, 12, 17]) * 24. + 9
        titles = [f'The splash area and drifted target distribution on {time_to_str(T1[0])}',
                  f'The search accomplishment on {time_to_str(T1[1])}',
                  f'The search accomplishment on {time_to_str(T1[2])}',
                  f'The search accomplishment on {time_to_str(T1[3])}',]
        
        ax0 = plt.subplot(gs[0])        
        dsmc.samples_advection()
        #dsmc.plot_samples(ax0, T1[0])    
        
        for iax in range(4):   
            
            ax = plt.subplot(gs[iax])
            t0 = T0[iax]
            t1 = T1[iax]            
            dsmc.plot_samples(ax, t1, ms=0.8, alpha=0.01)
            dsmc.format_axis(ax)
            ax.text(0, 1, labels[iax], 
                    fontsize=14,
                    fontweight = 'bold',
                    horizontalalignment='left',
                    verticalalignment='bottom', 
                    transform=ax.transAxes)
            ax.set_title(titles[iax], fontsize=10)
            
        dsmc.plot_initial_area(ax0)
        dsmc.format_axis(ax0)
            
            
        for iax in [1, 2, 3]:   
            
            ax = plt.subplot(gs[iax])
            t0 = T0[iax]
            t1 = T1[iax]  
            
            mrc = MRC[iax]
            d1 = 7 + mrc
            d2 = d1 + 10
            print(f'>>> Axis plot, [{t0}, {t1}], from {time_to_str(t0)} to {time_to_str(t1)}')
            
            cpconfig = cp.deepcopy(config)
            cpconfig['run'] = -1
            cpconfig['label'] = 'mrc.%d_s0.1_' % (mrc) # Temporary, only for testing
            
            #cpconfig['search_time'] = [d1*24 + 14, d2*24-6, 10, 1*60*24] # t1 [hours], t2 [hours], time step [seconds integer!], saving after how many time steps
            cpconfig['search_time'] = [d1*24 + 6, d2*24-14, 60, 10]
            #config['search_time'][3] = 1*10
            #start [minutes], end [minutes], number_of_agents
            cpconfig['search_regime'] =[[day*24*60 + 6*60, day*24*60 + 9*60, 10] for day in range(d1, d2)]
            #run_dir = config['results_dir'] + '/%s%s_run_%s/convergence.npz' % (config['label'], config['search_method'], 'full')
            cpconfig['uncertainty_sigma'] = 0
            
            dsmc_search = DSMC()
            dsmc_search.init_case(cpconfig)  
            #dsmc_search.velocity_initialization()  
            #dsmc_search.samples_advection()        
            dsmc_search.targets_advection()
            dsmc_search.plot_agents(ax, t1, ms=0.5)
            dsmc_search.plot_targets(ax, t1)
            del dsmc_search
            
        print(config['results_dir'])
        plt.savefig(config['results_dir'] + config['case'].lower().replace(' ', '_') + '_search.png')
        plt.close()
            
        del dsmc   
        
    else:
        full_run = m[-1] == 'f'
        runs = 1 if full_run else 100
        
        
        mrc = int(m[:-1]) if full_run else int(m)
        config['label'] = 'mrc.%d_s0.1_' % (mrc)
        # Start an end day of the search
        d1 = 7 + mrc
        d2 = d1 + 10
        
        config['search_time'] = [d1*24 + 6, d2*24-14, 60, 1*60*24] # t1 [hours], t2 [hours], time step [seconds integer!], saving after how many time steps
        # start [minutes], end [minutes], number_of_agents
        config['search_regime'] =[[day*24*60 + 6*60, day*24*60 + 9*60, 10] for day in range(d1, d2)]
    
            
        for r in range(runs):
            
            if full_run:
                config['run'] = -1
                run_dir = config['results_dir'] + '/%s%s_run_%s/convergence.npz' % (config['label'], config['search_method'], 'full')
                config['search_time'][3] = 10
            else:
                config['run'] = r
                run_dir = config['results_dir'] + '/%s%s_run_%03d/convergence.npz' % (config['label'], config['search_method'], config['run'])
                config['search_time'][3] = 60
                
            print(run_dir)
            if os.path.exists(run_dir):
                print('exists!')
                continue
            #input()
    
            dsmc = DSMC()     
            dsmc.detection_substeps = 25
            dsmc.init_case(config)
            dsmc.velocity_initialization()
            dsmc.samples_advection()
            dsmc.targets_advection()
            dsmc.run_search()
            del dsmc