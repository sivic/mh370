# -*- coding: utf-8 -*-
"""
Created on Wed Oct  8 10:27:35 2014

@author: stefan
"""

import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
from scipy.stats import norm
import os
from local_paths import *
import datetime


case = 'Area B'

if case == 'Area A':
    # Area A
    results_directory = area_a_results_dir_old
    plot_filename = 'area_a_comparison.pdf'

if case == 'Area B':
    # Area B
    results_directory = area_b_results_dir_old
    plot_filename = 'area_b_comparison.pdf'

methods = ['lwnmwrpredef', 'lwnmwrhull', 'dsmc', 'mdsmc']

labels = ['Lawnmower scenario 1', 'Lawnmower scenario 2', 'DSMC', 'mDSMC']
runs = 100

import seaborn as sns
sns.set_style("darkgrid")
current_palette = sns.color_palette()
#sns.palplot(current_palette)
print(current_palette)
#colors = ['orange', 'green', 'blue']
colors = current_palette
use_extracted_data = True
output_time_step = 1.0/24/6
bin_width = 1.0

import locale
locale.setlocale(locale.LC_ALL, 'en_US')
time0 = datetime.datetime(2014, 3, 1, 0, 0)
def time_to_str(time):    
    time = time0 + datetime.timedelta(hours=time)
    return time.strftime('%Y-%m-%d %H:%M')
    #return time.strftime('%b-%d %H:%M')

fig = plt.figure(figsize=(12, 3), dpi=200)
# if case == 'Area A':
#     fig = plt.figure(figsize=(12, 3))
# if case == 'Area B':
#     fig = plt.figure(figsize=(12, 3*0.8))
gs = gridspec.GridSpec(1, 2,
                       width_ratios=[1,1]
                       )
ax1 = plt.subplot(gs[0])
ax2 = plt.subplot(gs[1])
#ax2.set_ylim(0, 0.2)


for imeth, (method, lbl, clr) in enumerate(zip(methods, labels, colors)):

    print('\n\n\n%s\n' % lbl.upper())
    case_statistics_file = '%s/statistics_%s.npz' % (results_directory, method)
    print(case_statistics_file)
    
    if os.path.exists(case_statistics_file):
    #if False:
        print('Reading statistics file')
        print(case_statistics_file)
        npz = np.load(case_statistics_file)
        AT = npz['AT']
        time = npz['time']
        npz.close()
    else:
        print('Pharsing search results')
        AT = []
        for run in range(runs):
            fname = '%s/%s_run_%03d/convergence.npz' % (results_directory, method, run)
            #print(fname)
            if not os.path.exists(fname):
                break
            npz = np.load(fname)
            AT.append(npz['detection_history'])
            time = npz['search_time']/24
            npz.close()
            #print(run, AT[-1][-1])
    
        AT = np.array(AT)
        
        print('Saving statistics file')
        np.savez_compressed(case_statistics_file, AT = AT, time = time)
        print(case_statistics_file)
        
    #print(AT.shape)
    if np.size(AT) == 0:
        continue
           
        
    dt_avg = np.average(AT,0)
    dt_final = AT[:, -1]
    #time = np.array(range(len(dt_avg)))* output_time_step
    
    xx = np.linspace(np.floor(np.min(dt_final))-1, np.ceil(np.max(dt_final))+1, 200)
    #xx = np.linspace(np.floor(np.min(dt_final)/bin_width)*bin_width, np.ceil(np.max(dt_final)/bin_width)*bin_width, 200)
    mu, sigma = norm.fit(dt_final)
    pdf_fitted = norm.pdf(xx, mu, sigma)
    
    print('Number of runs: %d' % np.shape(AT)[0])
    print('mu:             %.2f' % mu)
    print('sigma:          %.2f' % sigma)
    
    dt_max = np.ceil(np.max(dt_final / 10.0) + 0.5) * 10.0
    
    
    """
    Left plot
    """
    tm = np.append(time, time[-1]+1)
    det = np.append(dt_avg, dt_avg[-1])
    at_min = np.append(np.min(AT, 0), np.min(AT, 0)[-1])
    at_max = np.append(np.max(AT, 0), np.max(AT, 0)[-1])
    #ax1.plot(tm, det, c = clr, lw = 1.2, label=lbl)
    #ax1.fill_between(tm, at_min, at_max, color=clr, alpha=0.2)  
    
    i1 = np.argmin(np.abs(np.floor(np.min(tm)) + 17/24 - tm))
    i2 = np.argmin(np.abs(np.floor(np.min(tm)) + 1 + 17/24 - tm))
    di = i2 - i1
    IX = np.arange(i1, np.size(tm), di, dtype=int)
    
    DYS = np.arange(np.size(IX))
    # _tm = tm[IX] + (imeth - 2) / 8
    xt = DYS + imeth * 0.15 - 0.225
    
    ax1.bar(xt, det[IX], 0.1, 
            color=clr, alpha=0.5, label=lbl,
            yerr=(det[IX]-at_min[IX], at_max[IX]-det[IX]), 
            error_kw=dict(ecolor='gray', lw=1, capsize=2, capthick=0.5))
    
    
    
    
    """
    Right plot
    """
    boxtxt = '$\sigma=%.2f$, $\mu=%.2f$' % (sigma, mu)
    pdf, bins, patches = ax2.hist(AT[:,-1], np.arange(xx[0], xx[-1]+1e-10, bin_width), alpha=0.5, color=clr, 
                                  weights=np.ones_like(AT[:,-1])/np.shape(AT)[0], density=False, histtype='bar', label=lbl + ' (' + boxtxt + ')')
    print('bins sum:', np.sum(pdf * np.diff(bins)))
    #ax2.plot([np.average(dt_final), np.average(dt_final)], [0, 0.3], 'b-', lw=2, label='Average')
    ax2.axvline(np.average(dt_final), c=clr, ls='-', lw=0.5)
    #ax2.axvline(np.median(dt_final), c=clr, ls='--', lw=2)
    ax2.fill_between(xx, pdf_fitted, ls='-', color=clr, alpha=0.2, lw=0.5)


    #ax2.set_xlim(xx[0], xx[-1])
    #fig.text(.6,.80,'$\sigma=%.2f$' % sigma, fontsize=16)
    #fig.text(.6,.75,'$\mu=%.2f$' % mu, fontsize=16)
    #props = dict(boxstyle='round', facecolor='white', alpha=1.0)
    #ax2.text(mu, 0.25, boxtxt, fontsize=14,
    #        verticalalignment='top', horizontalalignment='center', bbox=props)

#ax1.grid()
#ax1.set_xlabel('Time')
ax1.set_ylabel('Targets detection rate [%]', fontsize=8)
ax1.set_xticks(DYS)
#ax1.set_xticklabels(['\n'.join(time_to_str(x*24-8).split()) for x in tm[IX]], rotation=0, fontsize=8)
ax1.set_xticklabels([time_to_str(x*24-8).split()[0] for x in tm[IX]], rotation=0, fontsize=8)
ax1.set_xlim(DYS[0] - 0.5, DYS[-1] + 0.5)
if case == 'Area B':
    ax1.set_xlim(DYS[0] - 0.5, DYS[-1] + 3.5)

ax1.set_ylim(0, 100)
    
leg = ax1.legend(loc='best', fontsize=8)
leg.get_frame().set_linewidth(0)
leg.get_frame().set_alpha(0.8)
ax1.tick_params(axis='both', which='major', labelsize=8)
#ax2.grid()
ax2.set_xlabel('Targets detection rate [%]', fontsize=8)
ax2.set_ylabel('Frequency [-]', fontsize=8)
ax2.set_xlim(0, 100)
ax2.set_ylim(0, ax2.get_ylim()[1]*1.3)
leg = ax2.legend(loc='best', fontsize=8)
leg.get_frame().set_linewidth(0)
leg.get_frame().set_alpha(0.8)
ax2.tick_params(axis='both', which='major', labelsize=8)


ax1.text(0.0, 1.0, 'A', 
         weight = 'bold', fontsize=14,
         horizontalalignment='left',
         verticalalignment='bottom',
         transform=ax1.transAxes)
ax2.text(0.0, 1.0, 'B', 
         weight = 'bold', fontsize=14,
         horizontalalignment='left',
         verticalalignment='bottom',
         transform=ax2.transAxes)

plt.subplots_adjust(left=0.045, bottom=0.13, right=0.99, top=0.93, wspace=0.15)
# if case == 'Area A':
#     plt.subplots_adjust(left=0.045, bottom=0.13, right=0.99, top=0.93, wspace=0.15)
# if case == 'Area B':
#     plt.subplots_adjust(left=0.045, bottom=0.17, right=0.99, top=0.91, wspace=0.15)
    
plt.savefig(results_directory + '/' + plot_filename)




