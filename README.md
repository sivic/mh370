# MH370 DSMC search

This is the repository of the source code used in the research *Search strategy in a complex and dynamic environment: the MH370 case* by Stefan Ivić, Bojan Crnković, Hassan Arbabi, Sophie Loire, Patrick Clary and Igor Mezić published in Nature's *Scientific Reports* journal (DOI: 10.1038/s41598-020-76274-0 https://doi.org/10.1038/s41598-020-76274-0)

## Git and code access
I recommend using a git client (e. g. SmartGit https://www.syntevo.com/smartgit/) for accessing Git repository on GitLab or with a command `git clone https://gitlab.com/sivic/mh370.git`.

## Data access
The velocity data used in calculations is available on The Open Science Framework: https://osf.io/5jdhy/ . All other intemediate data and the rasults can be recreated using the code and velocity data.

## Python and needed modules
The easiest way to prepare all needed tools for running the code is to install Anaconda Python distribution (https://www.anaconda.com/download/) for **Python 3.x** (note that Python 2.x version is also available for download). Some of needed modules (libraries) already come with the default Anaconda installation:
- *NumPy*
- *SciPy*
- *Matplotlib*

Other needed modules need to be manually installed by running commands in **Anaconda prompt** (available in the start menu after Anaconda is installed, **it requires to be run with Administrator privileges**):
- *NetCdf4* module for reading `.nc` files (install with command `conda install -c conda-forge netcdf4`)
- *Shapely* module for geometric manipulations (install with command `conda install -c conda-forge shapely`)
- *Descartes* module for easier shape drawing (install with command `conda install -c conda-forge descartes`)
- *ghalton* module for Halton (quasi random) sequence generator (install with command `conda install -c jpn ghalton`)

Once all requirements are installed you need to prepare/modify `local_paths.py` according to your local paths to the data and results folders. Do this by copying existing `local_paths.py.txt` to `local_paths.py` and adjust.

You can start the *Area A case* by running `case_area_a.py` from *Spyder* IDE or from command line (*Anaconda prompt*): `python3 case_area_a.py`.

## Running the search simulations
The search simulation procedure is divided into multiple stages. Each stage stores calculated results or loads existing results in order to avoid running same calculations mutliple times. Some calculations are mutual for all runs for certain case and some differs for each run.

List of calculations:
- Case initalization
- Velociti initialization (loading `nc` files, trimming (both spatially and temporally), converting/adjusting to spherical coordinates, loading/saving to `npz` format, interpolation initialization). **Same for all runs!**
- Samples advection (initialization, advection, loading/saving advected samples, interpolation initialization). **Same for all runs!**
- Targets advection (initialization, advection, loading/saving advected targets, interpolation initialization). **Different for each run!**
- Search (Agent initialization, agent DSMC control, trail advection, target detection, saving the results). **Different for each run!**
- Postprocessing and visualizations
  - Figures for initial samples distribution
  - Frames for the search animation
  - Statistical plots along the search days and detection distribution at the end of the search
